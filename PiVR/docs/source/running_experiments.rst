PiVR has been developed by David Tadres and Matthieu Louis (`Louis Lab
<https://labs.mcdb.ucsb.edu/louis/matthieu/>`__).

.. Intended Audience: User who just wants to get an experiment started

.. _PiVR_Step_by_step_guide:

Step-by-step experimental guide
********************************

Read this guide if you:

#. Just want to run an experiment

#. Do not (yet) want to go too much into the details of how the
   tracking algorithm works.

.. _tracking guide:

Single animal tracking
======================

Press :ref:`here <TrackingLabel>` to learn how to select the Single
Animal tracking menu. Use this option to track single animals. It is
possible to provide a time-dependent stimulation. See :ref:`here
<VR_guide>` to learn how to present a single animal with a virtual
reality.

.. important::

    Several options will open a pop-up. You must close the pop-up in
    order to interact with the main window.

Select Organism
---------------

For proper identification and tracking you first have to define which
organism you are using. See :ref:`here <SelectOrganismLabel>` to
learn how to select your organism.

.. important::
    If you are using an animal that is **not** in the :ref:`List of
    Organisms <SelectOrganismLabel>`, please see
    :ref:`here <define_new_animal_label>`.

Define pixel per mm
-------------------

As you can put the camera at a variety of distances to the arena it
is **essential** to define the pixel per mm ratio. Please see
:ref:`here <PixMMLabel>` to learn how to do that.

.. _SettingUpArenaCamera:

Setting up the arena and the camera
-----------------------------------


It is important to understand how PiVR is able to detect and track an
animal in order to master this method.

The tracking software can only track what is in the field of view of
the camera. If your animal can leave the field of view of the camera,
tracking will stop and you will see an error.

.. figure:: /Figures/running_experiments/ExampleOutsideFOV.png
    :width: 100 %
    :alt: ExampleOutsideFOV.png

    The animal can run outside of the Field of View (FOV) during the
    experiment as the petri dish is not entirely visible using the
    camera. Adjust the dish that constrains animal movement to be in
    the FOV of the camera.

The algorithm works best if the background has as little contrast as
possible

.. figure:: /Figures/running_experiments/ExampleUnevenBackground.png
    :width: 100 %
    :alt: ExampleUnevenBackground.png

    The background is very uneven as the screw of the arena is
    visible (bottom left) and large portions of the image are just
    black while others are white. Adjust the camera and/or the arena
    so that the image only consists of the
    white background illumination (and the animal).

The algorithm works best if the animal has a **high contrast**
relative to the rest of the structure in the image.

.. figure:: /Figures/running_experiments/ExampleContrast.png
    :width: 100 %
    :alt: ExampleContrast.png

    Left: The fly can clearly be seen relative to the background.
    Right: The fly can be seen, but does not have a lot of contrast
    relative to the background. Try to :ref:`improve the image
    <AdjustImageLabel>` so that it looks more like the one on the left.

To set up PiVR to get an image as shown on the left, please follow
:ref:`these Instructions <SetUpOptimalImageLabel>`.

.. _AnimalDetectionGuideLabel:

Animal detection
----------------

The animal needs to be detected **before** the actual data collection
starts. It is also necessary to save a *Background* image for later
use. Ideally this Background image does not contain the animal that
should be tracked.

There are three different animal detection modes
(:ref:`press here how to select them <AnimalDetectionLabel>`), each with
it's  own advantages and drawbacks. See
:ref:`here<AnimalDetectionExplanationLabel>` for a an illustration of
the different methods.


#. **Standard - Mode#1:**  This mode will allow you to track a wide
   variety of animals without a lot of optimization of the
   camera nor image.

   We have used this mode to track adult flies

   **Advantages**:

   Should work with pretty much any organism if it has been defined
   before.

   Straightforward to run: Place animal, press start tracking, done

   **Disadvantages**:

   The Background image will have a partially visible animal.

   You can't align any virtual reality arenas relative to the  inital
   movement direction of the animal.

   See details :ref:`here <CodeExplanationMode1Label>`

#. **Pre-define Background - Mode#2:** If you need to define a cleaner
   background image, this mode can be useful. You take an image
   before the animal is placed, then add **only** the animal.

   We have used this mode to track zebrafish larvae

   **Advantages**:

   Can track any animal (probably better than Mode#1) if it has been
   defined before.

   Will give a clean background image

   **Disadvantages**:

   As with Mode#1 you can't align any virtual reality arenas relative
   to the inital movement direction of the animal.

   Extremely sensitive: If you have to move anything (such as holding
   up a lid of a petri dish where the animal is supposed to behave)
   this Mode probably won't work well.

   See details :ref:`here <CodeExplanationMode2Label>`

#. **Reconstruct Background by Stitching - Mode#3:**  Should produce the
   same clean background image as Mode#2. Only works if animal
   clearly stands out (has high contrast) in its local environement.

   We have used this mode with fruit fly larvae.

   **Advantages**:

   Allows the usage of aligned virtual reality arenas as the initial
   movement direction of the animal is detected.

   Will give a clean background image.

   **Disadvantages**:

   High contrast requirement hard to fullfill, therefore this mode
   does **not** work well with fast moving animals. Both because
   animals move quickly to the edge and because the area that must be
   taken into account by the detection algorithm increases with the
   speed of the animal.

   Can be difficult to use.

   See details :ref:`here <CodeExplanationMode3Label>`.

**To Summarize**

If you choose Mode 1 or Mode 3:

#. Place the animal in the arena, taking the :ref:`guide above
   <SettingUpArenaCamera>` into account.
#. Press 'Start Tracking'

If you choose Mode 2:

#. Prepare the arena *without* the animal, taking the :ref:`guide
   above <SettingUpArenaCamera>` into account.
#. Press 'Start Tracking'
#. Place the animal into the arena.
#. Press 'Ok'.

Animal tracking
---------------

After successful detection, the tracking algorithm starts following
the animal automatically without the user having to do anything.

While tracking is in progress, the preview window will overlay most
of the monitor and the GUI is not responsive.

There is no status bar available that could be shown during the
tracking.

It is also not possible to cancel the experiment using a
button.

After Animal Tracking
---------------------

Once tracking is finished (either because the animal was tracked for
the defined time or due to an error), the preview window will become
much smaller again.

After saving all the data (which can take a couple of seconds) the
GUI becomes responsive again.

.. _VR_guide:

Single animal Tracking with Virtual Reality
===========================================

Press :ref:`here <VRLabel>` to learn how to select the VR Arena option.

Besides selecting a virtual arena (as described in the :ref:`link
<VRLabel>`) everything is identical to :ref:`Single Animal Tracking
<tracking guide>`.

.. _Full_Frame_guide:

Taking full frame images
========================

Press :ref:`here <FullFrameLabel>` to learn how to select the single
image recording option.

This option is useful if you have several animals that you want to
record simultanously at a low framerate. It is possible to provide a
time-dependent stimulation. Compared to the :ref:`video option
<Video_guide>` the resulting images are uncompressed. The
disadvantage is the lower framerate and the additional hard drive
space necessary to save all the images.

#. Place your arena with the animals you would like to observe into
   the field of view of the camera.
#. Use :ref:`this guide<SetUpOptimalImageLabel>` to get the optimal
   image for your experiment.
#. Press 'Start Recording Images'

.. important::
    This option uses a lot of hard drive space. Make sure there is
    enough space left on your SD card before doing such an experiment.

.. _Video_guide:

Recording a video
=================

Press :ref:`here <VideoLabel>` to learn how to select the Video
Recording option.

This option is useful if you have several animals that you want to
record simultanously at a high (determined by your resolution, but at
640x480 you should be able to sustain >80 fps) framerates. It is
possible to provide a time-dependent stimulation. Compared to the
:ref:`full frame option <Full_Frame_guide>` the video will allow for
much higher framerates while using a fraction of hard drive space.
Videos are encoded in the h264 format. They can be converted into
any other format using ffmpeg_.

 .. _ffmpeg: https://ffmpeg.org/

#. Place your arena with the animals you would like to observe into
   the field of view of the camera.
#. Use :ref:`this guide<SetUpOptimalImageLabel>` to get the optimal
   image for your experiment.
#. Press 'Start Video Recording'
