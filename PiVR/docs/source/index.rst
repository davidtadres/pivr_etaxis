.. PiVR documentation master file, created by
   sphinx-quickstart on Tue Oct 16 19:17:07 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#########################################
PiVR: virtual reality for small animals
#########################################

The Raspberry  **Pi** based **V**\irtual **R**\eality system (PiVR) is
a virtual reality system for small animals. It has been developed by
David Tadres and Matthieu Louis (`Louis Lab
<https://labs.mcdb.ucsb.edu/louis/matthieu/>`__).

The tool has been published in PLOS Biology: `PiVR: An affordable and
versatile closed-loop platform to study unrestrained sensorimotor
behavior <https://doi.org/10.1371/journal.pbio.3000712>`__.

In addition, Scientific American featured PiVR in a delightful
`article <https://www.scientificamerican.com/article/fruit-flies-plug-into-the-matrix/>`__
accompanied by a fantastic `video <https://youtu.be/71SI7UScBU4>`__.

You can find a 1 hour presentation of PiVR hosted by
`World Wide Open Source <https://www.world-wide.org/Open-Source/>`__
on `youtube <https://www.youtube.com/watch?v=uG898FL421U>`__.


- The code is open source (`BSD license
  <https://choosealicense.com/licenses/bsd-3-clause/>`__)
- The source code for all the PiVR software can be found on `Gitlab
  <https://gitlab.com/LouisLab/PiVR/>`__
- You can also find a `Bug Tracker <https://gitlab
  .com/LouisLab/pivr/issues>`__ on Gitlab

.. figure:: Figures/other/PiVRLogo.jpg
   :width: 75%
   :alt: larva_banana.jpg

   Leonard the larva - always chasing that virtual banana smell.

What can PiVR do?
=================

PiVR has been used to create virtual odor realities for fruit fly larvae.
--------------------------------------------------------------------------

.. figure:: Figures/example_traces/larval_trajectory.png
   :width: 75%
   :alt: larval_trajectory.png

   Trajectory of a *Drosophila* larva in a virtual odor reality. The
   larva expresses the optogenetic tool Chrimson in the *Or42a*
   expressing olfactory sensory neuron.


   .. raw:: html

       <iframe width="75%"
       src="https://www.youtube.com/embed/C9fYz0iN8w0" frameborder="0"
       allow="accelerometer; autoplay;
       encrypted-media; gyroscope; picture-in-picture"
       allowfullscreen></iframe>

|

PiVR has also been used to create virtual taste realities for adult fruit flies.
---------------------------------------------------------------------------------

.. figure:: Figures/example_traces/fly_trajectory.png
   :width: 75%
   :alt: fly_trajectory.png\

   Trajectory of an adult *Drosophila* fly in a virtual taste reality.
   The fly expresses the optogenetic tool Chrimson in the *Gr66a*
   expressing sensory neurons.

.. raw:: html

    <iframe width="75%"
    src="https://www.youtube.com/embed/aow-CmUcT_o" frameborder="0"
    allow="accelerometer; autoplay; encrypted-media;
    gyroscope; picture-in-picture" allowfullscreen></iframe>

|

PiVR was also used to create a virtual light bulb for a number of animals, including larval zebrafish.
-------------------------------------------------------------------------------------------------------

.. figure:: Figures/example_traces/fish_trajectory.png
   :width: 75%
   :alt: fish_trajectory.png

   Trajectory of a zebrafish (D. rerio) larva exposed to a virtual
   white light source.

.. raw:: html

    <iframe width="75%"
    src="https://www.youtube.com/embed/7J5thhZ7Sro" frameborder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope;
    picture-in-picture" allowfullscreen></iframe>

|

PiVR is also able to create dynamic virtual gradients
-----------------------------------------------------

While it is often convenient to present static virtual gradients (see
examples above) animals usually have to navigate an environment that
is changing over time. PiVR is able to present animals with dynamic
virtual realities.

We presented *Drosophila* larva expressing the optogenetic tool
Chrimson in the Or42a olfactory sensory neuron with a dynamic odor
plume based on the measurement of a real odor plume (`Álvarez-Salvado et.
al., <https://elifesciences.org/articles/37815>`__). PiVR thus
enables researchers to study how *Drosophila* larvae are orienting
themselves in a more naturalistic environments.

.. raw:: html

    <iframe width="75%"
    src="https://www.youtube.com/embed/RGAta8VqIlw" frameborder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope;
    picture-in-picture" allowfullscreen></iframe>

|

Sounds great. I want one! How?
==============================

PiVR has been designed to make building one as easy as possible so
that you do not spend a lot of time building the setup and spend more
time running experiments.

Please follow the :ref:`PIVR Hardware Manual` to see step-by-step
instructions on how to build your own PiVR setup.

Don't worry, it's not hard and it won't take too long. Please see the
timelapse video below for an example of one setup being built: from
3D printing to running experiments!

.. raw:: html

    <iframe width="75%"
    src="https://www.youtube.com/embed/w5tIG6B6FWo" frameborder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope;
    picture-in-picture" allowfullscreen></iframe>

|

I've got a setup. How do I use it?
===================================

If you are a first time user, check out the :ref:`Step-By-Step
<PiVR_Step_by_step_guide>` Guide which will walk you through each of
the four recording modes:

#. :ref:`Tracking single animal <tracking guide>`

#. :ref:`Virtual Reality experiments <VR_guide>`

#. :ref:`Image Sequence Recording <Full_Frame_guide>`

#. :ref:`Video Recording <Video_guide>`

You have just run an experiment. What to make of the output data?
See :ref:`here<Output>` to understand what each output file means and
what it contains.

To see how PiVR can help you analyse data check out the
:ref:`tools<tools>` available on the PC version of PiVR.

Advanced documentation
----------------------

If you are running into trouble with the closed loop tracking, please
head over to :ref:`How to simulate real time tracking <debug_tracking>`.

If you want to track an animal that is not available under
:ref:`Select Animal <SelectOrganismLabel>`, please read the
:ref:`How to define a new animal <define_new_animal_label>` chapter.

If you want to understand what each button in the GUI is doing,
please see the :ref:`PiVR Software Manual <PiVR Software Manual>`.

If you want to gain a high-level understanding on how the code
identifies the animal and tracks them please read the
:ref:`Code Explanation <CodeExplanationLabel>`

The annotated source code can be found :ref:`here <PiVR source code>`
and on the `Gitlab page <https://gitlab.com/LouisLab/PiVR/>`__.


Content
=======

.. toctree::
   :maxdepth: 1
   :numbered:

   self
   manual_hardware
   BOM
   running_experiments
   code_explanation
   manual_software
   output
   tools
   advanced_topics
   software_installation
   software_documentation
   benchmarking
   FAQ
   contact
   PiVR_mentioned

.. Indices and tables
   ==================
   * :ref:`intro`
   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
   * :ref:`PiVR source code`
   * :ref:`PiVR Software Manual`
   * :ref:`PIVR Hardware Manual`
   * :ref:`PiVR_Step_by_step_guide`
   * :ref:`Output`
   * :ref:`tools`
   * :ref:`advanced_topics`
   * :ref:`software_installation`
   * :ref:`benchmarking`
   * :ref:`FAQ`
   * :ref:`contact`
   * :ref:`PiVR_mentioned`
   Note: I've deactivated this as it doesn't look great



