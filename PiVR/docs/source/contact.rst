PiVR has been developed by David Tadres and Matthieu Louis (`Louis Lab
<https://labs.mcdb.ucsb.edu/louis/matthieu/>`__).

.. _contact:

Contact
===================

Questions?
----------

If something does not work as you expect, one of the best ways to get
information is to open an "issue" on the `PiVR gitlab repository
<https://gitlab.com/LouisLab/PiVR/issues>`__.

Found a Bug?
------------

Please open an "issue" on the `PiVR gitlab repository
<https://gitlab.com/LouisLab/PiVR/issues>`__. It is imperative to
give as much information in when you encountered the bug. Ideally,
you are able to let us know exactly how to reproduce the bug. At
least you need to let us know:

#. The OS (on the Raspberry Pi it is probably Raspbian)
#. What exactly you were doing when the error occured.

Other questions (scientific, technological, copyright...)
------------------------------------------------------------------------

Please contact the corresponding author of this work, `Matthieu Louis
<https://www.mcdb.ucsb.edu/people/faculty/matthieu-louis>`__.
