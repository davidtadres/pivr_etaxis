PiVR has been developed by David Tadres and Matthieu Louis (`Louis Lab
<https://labs.mcdb.ucsb.edu/louis/matthieu/>`__).

.. _PIVR Hardware Manual:

Build your own PiVR
===================

It doesn't take long and it isn't too hard to build PiVR. See this
timelapse video where a PiVR setup is being 3D printed, the PCB soldered
and then built.

.. raw:: html

    <iframe width="75%"
    src="https://www.youtube.com/embed/w5tIG6B6FWo" frameborder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope;
    picture-in-picture" allowfullscreen></iframe>

Standard and High Powered version
---------------------------------

PiVR comes in two versions:

#. :ref:`The Standard version<PiVR Standard Version>`:
   LED strips are used for both the backlight and stimulation light.
   We have measured around :math:`2{\mu}W/mm^2` for red light
   intensity and around :math:`8000 Lux` when measuring white light
   intensity. Please see the :ref:`Bill of Materials <BOM>` for
   components you must buy to build the setup.

#. :ref:`The High Powered Version<PiVR HP Version>`: The
   stronger LEDs require not only a different arena but also
   dedicated LED drivers which provide a constant current. We have
   measured light intensities in the order of :math:`40 -
   50{\mu}W/mm^2` for the red LEDs.

.. _PiVR Standard Version:

Building the Standard version
-----------------------------

.. Warning::

    You will be handling sensitive electronic equipment. As you might
    be electrically charged, always touch something metallic that is
    connected to the ground before unpacking anything from an
    electrostatical shielding bag. A radiator will do fine.

.. note::

    You can of course change the order of how to build the different
    parts. I build them in this order as there are necessary delays
    such as installing the operation system on the Raspberry Pi.

.. note::

    We used an Ultimaker 3 with a Ultimaker Print Core AA (0.8mm) to
    print the PLA and a Ultimaker Print Core BB (0.8) to print the
    PVA support material. STL files were converted using Ultimaker
    CURA software.

#. 3D print each part in the folder PiVR/Hardware/3D printer files.
   We used an Ultimaker 3 and printed with Standard PLA. We used PVA as
   support material. For best results print Casing, CameraHolder,
   TowerBottom and TowerTop with support material.

    .. figure:: Figures/manual_hardware/standard/1_3D_printed_parts.jpg
       :width: 100 %
       :alt: 1_3D_printed_parts.jpg

#. Obtain the Printed Circuit Board (PCB). Find the blueprint in
   PiVR\Hardware\PCB. I have used the software
   `Fritzing <http://fritzing.org/home/>`_ to design the board. I
   have used the company  `Aisler.net <https://aisler.net/>`_ to print
   the circuit boards.

    .. figure:: Figures/manual_hardware/standard/2_0_PCB.jpg
       :width: 100 %
       :alt: 2_0_PCB.jpg

#. Get the following items to solder the PCB board:

    .. important::

        Before touching electrical components always touch something
        metalic that is connected to the ground. For example a radiator.

    #. Four (4) Transistors: 30N06L
    #. One (1) GPIO Header
    #. One (1) Barrel connector, 5.5mm Jack with 2.1mm center pole
       diameter
    #. Four (4) Barrel connectors, 3.5mm Jack with 1.35mm center pole
    #. Four (4) :math:`10k{\Omega}` resistors
    #. Break off a 5 pin stretch from the Breakaway Headers. This can
       be done using scissors or pliers.

   .. figure:: Figures/manual_hardware/standard/2_7_PCB_components.jpg
      :width: 100 %
      :alt: 2_7_PCB_components.jpg

#. Solder the components on the PCB. See
   :ref:`this section<detailed_soldering_instructions>` for
   detailed soldering instructions.

    .. important::

        The correct **orientation** of the **GPIO header** and the
        **Transistors** is crucial for PiVR to work correctly.

   .. figure:: Figures/manual_hardware/standard/3_solderedPCB.jpg
      :width: 100 %
      :alt: 3_solderedPCB.jpg

#. Cut off the excess wire of the resistors and the Transistors, e.g.
   with scissors.

   .. figure:: Figures/manual_hardware/standard/4_PCB_removed_wire.jpg
      :width: 100 %
      :alt: 4_PCB_removed_wire.jpg

#. Unpack the Touchscreen, remove the stand-off screws. Attach the
   monitor cable that came with the Touchscreen and the 4"
   5 pin cable.

    .. important::

        The monitor cable must be inserted in the correct orientation.
        When you look into the receptacle you'll see that only one
        side has connectors. Make sure that you insert the cable's
        connectors on the same side.

    .. important::

        Note the orientation of the 4" 5pin cable! Left is (+) while
        Right is (-).

   .. figure:: Figures/manual_hardware/standard/6_Touchscreen.jpg
      :width: 100 %
      :alt: 6_Touchscreen.jpg

#. Place the Casing on top of the Touchscreen (it will only fit in
   the shown orientation). Organize the 4" 5pin cable and the
   monitor cable as shown in the picture. Use the M2.5x10mm screws to
   fix the casing to the touchscreen.


   .. figure:: Figures/manual_hardware/standard/7_2_Casing_on_Touchscreen.jpg
      :width: 100 %
      :alt: 7_2_Casing_on_Touchscreen.jpg

#. Prepare the SD card: Format the SD card using SD Formatter and load with
   NOOBs installation files as instructed
   `here <https://www.raspberrypi.org/help/noobs-setup/2/>`_:

#. Connect monitor cable with the Raspberry Pi (with inserted SD
   card). Again, make sure you insert the cable in the correct
   orientation. Use M2.5x10 screws to attach the Raspberry Pi to the
   Casing.

   .. figure:: Figures/manual_hardware/standard/8_0_Raspberry_monitor_cable.jpg
      :width: 100 %
      :alt: 8_0_Raspberry_monitor_cable.jpg

#. Attach the PCB board on the right side of the casing using M2.5x10mm
   screws. Plug the 4" 5pin cable into the PCB in the **correct**
   orientation as shown!

   .. figure:: Figures/manual_hardware/standard/10_PCB_RPi_on_casing.jpg
      :width: 100 %
      :alt: 10_PCB_RPi_on_casing.jpg

#. Use the GPIO Ribbon cable to connect the PCB board with the
   Raspberry Pi. Thread the long camera cable through the slit as
   shown in the image below. Connect it to the Raspberry Pi Camera port.

   .. figure:: Figures/manual_hardware/standard/12_Casing_stuffed.jpg
      :width: 100 %
      :alt: 12_Casing_stuffed.jpg

#. Attach the Backside to the CasingPedestal with M2.5x10 screws.

   .. figure:: Figures/manual_hardware/standard/13_Backside_attached_to_Pedestal.jpg
      :width: 100 %
      :alt: 13_Backside_attached_to_Pedestal.jpg

#. Slide the Backside into the Casing

#. Drop a 2.5mm nut in each hole in the TowerPedestal. Use the M2.5x10
   screws to attach the TowerBottom to the Tower Pedestal

   .. figure:: Figures/manual_hardware/standard/14_TowerPedestal_attached.jpg
      :width: 100 %
      :alt: 14_TowerPedestal_attached.jpg

#. Using a hammer, drive the dowel pins into the TowerBottom. Then
   attach the TowerTop to it. In principle you can stack more TowerTops
   on top.

   .. figure:: Figures/manual_hardware/standard/15_TowerBottom_Dowel.jpg
      :width: 100 %
      :alt: 15_TowerBottom_Dowel.jpg

#. Attach the 800nm Longpass Filter to the Camera using Parafilm. It
   is best to wear gloves for this step.

   .. figure:: Figures/manual_hardware/standard/16_Camera_with_LP_filter.jpg
      :width: 100 %
      :alt: 16_Camera_with_LP_filter.jpg

#. Thread the camera cable from the Casing through the slit in the
   TowerBottom and through the slit of the Camera Holder.

    .. important::

        Note the orientation to avoid having to curl the camera cable
        in the camera holder

   .. figure:: Figures/manual_hardware/standard/17_Threading_of_Cam_cable.jpg
      :width: 100 %
      :alt: 17_Threading_of_Cam_cable.jpg

#. Attach the Camera Cable to the Camera in the **correct**
   orientation. Then screw the camera to the Camera Holder using the
   M2.5x10 screws. It is **not** necessary to fixate the screws with
   nuts!

   .. figure:: Figures/manual_hardware/standard/18_Camera_attached_to_CamHolder.jpg
      :width: 100 %
      :alt: 18_Camera_attached_to_CamHolder.jpg

#. Drop a 2.5mm nut in the hole in the Camera holder and use it to
   fasten the M2.5x10 screw. Then attach the CameraHolder to the
   Tower.

   .. figure:: Figures/manual_hardware/standard/19_PiVR_complete.jpg
      :width: 100 %
      :alt: 19_PiVR_complete.jpg

#. Plug the 5V power source into the micro USB slot of the Raspberry
   Pi (right side). After a couple of seconds the monitor should
   display a colorful image. Then the operating system installation
   will commence. Select the *Recommened* OS.

   .. figure:: Figures/manual_hardware/standard/20_Install_Raspian.jpg
      :width: 100 %
      :alt: 20_Install_Raspian.jpg

#. On the first startup the OS asks a couple of questions. The
   most important one is the language - make sure you choose the
   correct Keyboard layout. Make sure the Raspberry Pi is
   connected to the internet and download the
   :download:`PiVR installation file <../install_PiVR.sh>`

#. Open the terminal. Then change directory to the 'Downloads' folder
   (or wherever you downloaded the file) and type::

      bash install_PiVR.sh

   .. figure:: Figures/manual_hardware/standard/21_install_PiVR_software.jpg
      :width: 100 %
      :alt: 21_install_PiVR_software.jpg

#. Now the arena will be built. In the folder PiVR/Lasercutter_Files/
   you can find two vector graphic files that can be used to Lasercut
   a 20cm or 30cm arena, circular holes for M8 screws and small lines
   indicating the distance of 1cm on each side. For one arena you
   will need two acrylic plates.

   .. figure:: Figures/manual_hardware/standard/22_Arena_overview.jpg
      :width: 100 %
      :alt: 22_Arena_overview.jpg

#. Cut the 850nm (infrared) LED strips to the desired length (e.g. 20cm
   on a 20cm Arena) and attach them to the arena. You can choose the
   horizontal distance yourself. I usually use a distance of 3cm.

    .. important::

        It will make soldering much easier if you make sure the
        (+) and the (-) between the LED strips is consistent!

   Solder (+) to (+) from one side of the arena to the other. Then
   repeat for (-).

   .. figure:: Figures/manual_hardware/standard/23_soldered_arena.jpg
      :width: 100 %
      :alt: 23_soldered_arena.jpg

#. Attach the female Barrel Jack to a convenient copper dot on the
   LED strip. Then fix the Female Barrel Jack using super
   glue/hot glue gun. Make sure you are leaving space for the M8
   screw to pass through.

    .. important::

        Usually the red wire of the Barrel Jack indicates (+)!

   .. figure:: Figures/manual_hardware/standard/24_arena_barrel_jack_attached.jpg
      :width: 100 %
      :alt: 24_arena_barrel_jack_attached.jpg

#. If you want to add a Stimulation LED strip (e.g. Red Stimulation
   light), just attach it in between the infrared LED strips, solder
   it as you did the 850nm LED strips and attach the female Barrel
   connector at a convenient location and fix it using the hot glue
   gun.

   .. figure:: Figures/manual_hardware/standard/25_arena_second_strip.jpg
      :width: 100 %
      :alt: 25_arena_second_strip.jpg

#. After inserting the M8 screws into the holes, thread a M8 nut on
   each of the screws about 2cm in. Put the second plate on top of
   the first and fasten it by threading a second M8 nut on top of the
   plate. Make sure the top plate is completely level by using a spirit
   level!

   .. figure:: Figures/manual_hardware/standard/26_second_plate_fastened.jpg
      :width: 100 %
      :alt: 26_second_plate_fastened.jpg

#. To connect PiVR with the arena a cable needs to be constructed.
   You will need two 5.5mm Male Barrel Jack, two 3.5 male solderable
   Barrel Jacks and around 20 Gauge wire.

   .. figure:: Figures/manual_hardware/standard/27_cables_overview.jpg
      :width: 100 %
      :alt: 27_cables_overview.jpg

#. See :ref:`here<detailed_soldering_instructions_cable>` how to
   construct a cable in detail. Briefly, start by cutting a
   reasonable long piece of the wire, e.g. 50cm, but this depends on
   your application. Attach one side of the cable to the 3.5mm barrel
   jack. You may solder it, but be careful to only use minute amounts of
   solder. Then solder on the 5.5mm barrel jack on the other side,
   fixing it using the shrinking tubes.

   .. figure:: Figures/manual_hardware/standard/28_cable_done.jpg
      :width: 100 %
      :alt: 28_cable_done.jpg

#. Start the PiVR software by double clicking on the shortcut on the
   Desktop. Under 'Options' Select
   :ref:`'Optimize Image' <AdjustImageLabel>`.

   .. figure:: Figures/manual_hardware/standard/29_Starting_Software.jpg
      :width: 100 %
      :alt: 29_Starting_Software.jpg

#. Connect the 12V power source (make sure you have an appropriate
   Ampere rating for the amount of LEDs you use!) to the 5.5mm Input
   on the setup. Do not plug it into the wall socket just yet!

   .. Warning::

       Do not plug the 12V power source into the wall socket while you
       are handling the arena wires.

   Then connect the 3.5mm cable with the appropriate receptacle
   closest to the 5.5mm plug. Then plug the other side into the IR
   LEDs on the arena.

   Now you can plug in the 12V power source into the wall socket.

   .. figure:: Figures/manual_hardware/standard/30_connecting_IR_cables.jpg
      :width: 100 %
      :alt: 30_connecting_IR_cables.jpg

#. Turn the camera on ('Cam On'). Then move the 'Backlight Intensity'
   slider to something like 400'000. You should see how the image on
   the top left of the screen lights up.

   .. note::

      Since the camera has a 800nm Longpass filter you shouldn't see
      anything in the camera preview as long as the infrared light of
      the arena is off, **unless** you have a strong source of
      infrared radiation around, e.g. the Sun.

   .. figure:: Figures/manual_hardware/standard/30_1_testing_IR.jpg
      :width: 100 %
      :alt: 30_1_testing_IR.jpg


#. Connect a second 3.5mm cable just below the first. The other side
   goes into the first Stimulation Light in the arena.

   .. figure:: Figures/manual_hardware/standard/31_connecting_redlight_cable.jpg
      :width: 100 %
      :alt: 31_connecting_redlight_cable.jpg

#. When moving the slider labelled 'Channel 1' the stimulation LED
   should light up.

   .. figure:: Figures/manual_hardware/standard/31_1_testing_channel1.jpg
      :width: 100 %
      :alt: 31_1_testing_channel1.jpg

#. If these tests have been successful, congratulations, you've built
   your own PiVR.

.. _detailed_soldering_instructions:

Detailed PCB soldering instructions (Standard Version)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. Warning::

   Important! Make sure that the pins are not connected due to
   imprecise soldering!

#. I prefer to solder the components on the PCB board in this
   particular sequence as I find it easiest to keep the components in
   place. Otherwise there is no reason to not solder components in
   any sequence you prefer!

#. To solder the PCB board you will need the following elements:

    #. Four (4) Transistors: 30N06L

       .. figure:: Figures/manual_hardware/standard/2_1_transistors.jpg
          :width: 100 %
          :alt: 2_1_transistors.jpg

    #. One (1) GPIO Header

       .. figure:: Figures/manual_hardware/standard/2_2_GPIO-Header.jpg
          :width: 100 %
          :alt: 2_2_GPIO-Header.jpg

    #. One (1) Barrel connector, 5.5mm Jack with 2.1mm center pole
       diameter

       .. figure:: Figures/manual_hardware/standard/2_3_Jack5_5.jpg
          :width: 100 %
          :alt: 2_3_Jack5_5.jpg

    #. Four (4) Barrel connectors, 3.5mm Jack with 1.35mm center pole

       .. figure:: Figures/manual_hardware/standard/2_4_Jack3_5.jpg
          :width: 100 %
          :alt: 2_4_Jack3_5.jpg

    #. Four (4) :math:`10k{\Omega}` resistors

       .. figure:: Figures/manual_hardware/standard/2_5_resistors.jpg
          :width: 100 %
          :alt: 2_5_resistors.jpg

    #. Break off a 5 pin stretch from the Breakaway Headers. This can
       be done using scissors or pliers.

       .. figure:: Figures/manual_hardware/standard/2_6_Headers.jpg
          :width: 100 %
          :alt: 2_6_Headers.jpg

#. Take one of  the small barrel plug and place it into the
   leftmost possible spot on the PCB board as shown.

   .. figure:: Figures/manual_hardware/standard/S_1.jpg
      :width: 100%
      :alt: S_1.jpg

#. Flip the PCB board while holding the small barrel plug in
   place. By placing it on the table, it should not move and allow
   you to easily solder the three pins of the barrel plug to the PCB
   as shown.

   .. figure:: Figures/manual_hardware/standard/S_2.jpg
      :width: 100%
      :alt: S_2.jpg

#. Continue to solder the other three small barrel plugs, one by one,
   onto the the PCB board.

   .. figure:: Figures/manual_hardware/standard/S_3.jpg
      :width: 100%
      :alt: S_3.jpg

#. Next, place the GPIO header in **exactly** the orientation shown in
   the image below onto the PCB board.

   .. figure:: Figures/manual_hardware/standard/S_4.jpg
      :width: 100%
      :alt: S_4.jpg

#. Flip the PCB board with the GPIO header around. As it now stands
   on the table it should be easy to solder. You do not have to
   solder every single pin to the PCB (minimum is shown on top
   picture) but it is recommended to solder more, ideally all. **Be
   sure the solder between the pins does not touch**

   .. figure:: Figures/manual_hardware/standard/S_5.jpg
      :width: 100%
      :alt: S_5.jpg

#. Place the 5-pin stretch of breakaway headers into the holes on
   the far right on the PCB. Make sure to place them in the correct
   orientation as shown in the picture (short side on the 'back' of
   the PCB).

   .. figure:: Figures/manual_hardware/standard/S_6.jpg
      :width: 100%
      :alt: S_6.jpg

#. Flip the PCB with the 5-pin stretch of breakaway headers around
   and solder the header to the PCB board.

   .. figure:: Figures/manual_hardware/standard/S_7.jpg
      :width: 100%
      :alt: S_7.jpg

#. Now to the resistors. Place a resistor in the indicated position:

   .. figure:: Figures/manual_hardware/standard/S_8.jpg
      :width: 100%
      :alt: S_8.jpg

#. Flip the PCB board around. If the resistor falls out, just fixate
   it by bending the wire as indicated here. Then solder it the the
   PCB board.

   .. figure:: Figures/manual_hardware/standard/S_9.jpg
      :width: 100%
      :alt: S_9.jpg

#. Do the same for the other three resistors.

   .. figure:: Figures/manual_hardware/standard/S_10.jpg
      :width: 100%
      :alt: S_10.jpg

#. Now take the large barrel connector and place it on the PCB at the
   indicated position

   .. figure:: Figures/manual_hardware/standard/S_11.jpg
      :width: 100%
      :alt: S_11.jpg

#. Flip the PCB board around and solder the large barrel connector to
   the PCB board.

   .. figure:: Figures/manual_hardware/standard/S_12.jpg
      :width: 100%
      :alt: S_12.jpg

#. Next, take one of the transistors and place it **exactly** as
   shown onto the PCB board.

   .. figure:: Figures/manual_hardware/standard/S_13.jpg
      :width: 100%
      :alt: S_13.jpg

#. Flip the PCB board around and solder the transistor to the board.
   Make sure the solder of the different pins does not touch the
   contact of one of the other pins! **Warning: Transistors are more
   heat sensitive compared to the other components you have used so
   far. Make sure to not let them heat up too much!**

   .. figure:: Figures/manual_hardware/standard/S_14.jpg
      :width: 100%
      :alt: S_14.jpg

#. Do the same for the other three transistors.

   .. figure:: Figures/manual_hardware/standard/S_15.jpg
      :width: 100%
      :alt: S_15.jpg

#. You must get rid of the elongated wiring of the transistors and
   especially the resistors as not doing so will 1) increase risk of
   shorting components and 2) it will physically be very hard to put
   the PCB board into the casing. While it is probably best to use
   the shown wire clipper, it is also possible to do that using
   normal scissor.

   .. figure:: Figures/manual_hardware/standard/S_16.jpg
      :width: 100%
      :alt: S_16.jpg

#. Congratulations - You have finished soldering your very own PCB
   board to build PiVR!

.. _PiVR HP Version:

Building the High Powered Version
---------------------------------

.. Warning::

   **PLEASE** avoid looking directly into the LED light. It might
   damage your eyes.

   If you are in a situation where you might accidentally look into
   the LED light at 100% power, please consider purchasing eye saftey
   googles for the wavelength you are using, for example from `here
   <https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=762>`__.

.. Warning::

    You will be handling sensitive electronic equipment. As you might
    be electrically charged, always touch something metallic that is
    connected to the ground before unpacking anything from an
    electrostatical shielding bag. A radiator will do fine.

.. note::

    You can of course change the order of how to build the different
    parts. I build them in this order as there are necessary delays
    such as installing the operation system on the Raspberry Pi.

.. note::

    We used an Ultimaker 3 with a Ultimaker Print Core AA (0.8mm) to
    print the PLA and a Ultimaker Print Core BB (0.8) to print the
    PVA support material. STL files were converted using Ultimaker
    CURA software.

#. 3D print each part in the folder PiVR/Hardware/3D printer files.
   We used an Ultimaker 3 and printed with Standard PLA. We used PVA as
   support material. For best results print Casing, CameraHolder,
   TowerBottom and TowerTop with support material.

    .. figure:: Figures/manual_hardware/standard/1_3D_printed_parts.jpg
       :width: 100 %
       :alt: 1_3D_printed_parts.jpg

#. Obtain the Printed Circuit Board (PCB). Find the blueprint in
   PiVR\Hardware\PCB. I have used the company `Aisler.net
   <https://aisler.net/>`_ to print the circuit boards.

    .. figure:: Figures/manual_hardware/high_power/2_1_PCB_overview.jpg
       :width: 100 %
       :alt: 2_1_PCB_overview.jpg

#. Get the following items to solder the PCB board:

    .. important::

        Before touching electrical components always touch something
        metalic that is connected to the ground. For example a radiator.

    #. Three (3) LED driver "MiniPuck", 700mA (or whatever strength
       you need)
    #. One (1) Transistors: 30N06L
    #. One (1) GPIO Header
    #. One (1) Barrel connector, 5.5mm Jack with 2.1mm center pole
       diameter
    #. Four (4) Barrel connectors, 3.5mm Jack with 1.35mm center pole
    #. One (1) :math:`10k{\Omega}` resistors
    #. Break off a 5 pin stretch from the Breakaway Headers. This can
       be done using scissors or pliers.

   .. figure:: Figures/manual_hardware/high_power/2_2_PCB_components_HP.jpg
      :width: 100 %
      :alt: 2_7_PCB_components_HP.jpg

#. Solder the components on the PCB. See
   :ref:`this section<detailed_soldering_instructions_HP>` for
   detailed soldering instructions.

    .. important::

        The correct **orientation** of the **GPIO header** and the
        **Transistor** is crucial for PiVR to work correctly.

   .. figure:: Figures/manual_hardware/high_power/3_solderedPCB_HP.jpg
      :width: 100 %
      :alt: 3_solderedPCB_HP.jpg

#. Cut off the excess wire of the resistors and the Transistors, e.g.
   with scissors.

   .. figure:: Figures/manual_hardware/high_power/4_PCB_removed_wire_HP.jpg
      :width: 100 %
      :alt: 4_PCB_removed_wire_HP.jpg

#. Unpack the Touchscreen, remove the stand-off screws. Attach the
   monitor cable that came with the Touchscreen and the 4"
   5 pin cable.

    .. important::

        The monitor cable must be inserted in the correct orientation.
        When you look into the receptacle you'll see that only one
        side has connectors. Make sure that you insert the cable's
        connectors on the same side.

    .. important::

        Note the orientation of the 4" 5pin cable! Left is (+) while
        Right is (-).

   .. figure:: Figures/manual_hardware/standard/6_Touchscreen.jpg
      :width: 100 %
      :alt: 6_Touchscreen.jpg

#. Place the Casing on top of the Touchscreen (it will only fit in
   the shown orientation). Organize the 4" 5pin cable and the
   monitor cable as shown in the picture. Use the M2.5x10mm screws to
   fix the casing to the touchscreen.


   .. figure:: Figures/manual_hardware/standard/7_2_Casing_on_Touchscreen.jpg
      :width: 100 %
      :alt: 7_2_Casing_on_Touchscreen.jpg

#. Prepare the SD card: Format the SD card using SD Formatter and load with
   NOOBs installation files as instructed
   `here <https://www.raspberrypi.org/help/noobs-setup/2/>`_:

#. Connect monitor cable with the Raspberry Pi (with inserted SD
   card). Again, make sure you insert the cable in the correct
   orientation. Use M2.5x10 screws to attach the Raspberry Pi to the
   Casing.

   .. figure:: Figures/manual_hardware/standard/8_0_Raspberry_monitor_cable.jpg
      :width: 100 %
      :alt: 8_0_Raspberry_monitor_cable.jpg

#. Attach the PCB board on the right side of the casing using M2.5x10mm
   screws. Plug the 4" 5pin cable into the PCB  in the **correct**
   orientation as shown!

   .. figure:: Figures/manual_hardware/high_power/9_PCB_RPi_on_casing_HP.jpg
      :width: 100 %
      :alt: 9_PCB_RPi_on_casing_HP.jpg

#. Use the GPIO Ribbon cable to connect the PCB board with the
   Raspberry Pi. Thread the long camera cable through the slit as
   shown in the image below. Connect it to the Raspberry Pi Camera port.

   .. figure:: Figures/manual_hardware/high_power/10_Casing_stuffed.JPG
      :width: 100 %
      :alt: 10_Casing_stuffed.JPG

#. Attach the Backside to the CasingPedestal with M2.5x10 screws.

   .. figure:: Figures/manual_hardware/standard/13_Backside_attached_to_Pedestal.jpg
      :width: 100 %
      :alt: 13_Backside_attached_to_Pedestal.jpg

#. Now just slide the Backside (with attached CasingPedestal) into the
   Casing.

   .. figure:: Figures/manual_hardware/high_power/11_Backside_into_casing.JPG
      :width: 100 %
      :alt: 11_Backside_into_casing.JPG


#. Drop a 2.5mm nut in each hole in the TowerPedestal. Use the M2.5x10
   screws to attach the TowerBottom to the Tower Pedestal

   .. figure:: Figures/manual_hardware/standard/14_TowerPedestal_attached.jpg
      :width: 100 %
      :alt: 14_TowerPedestal_attached.jpg

#. Using a hammer, drive the dowel pins into the TowerBottom. Then
   attach the TowerTop to it. In principle you can stack more TowerTops
   on top.

   .. figure:: Figures/manual_hardware/standard/15_TowerBottom_Dowel.jpg
      :width: 100 %
      :alt: 15_TowerBottom_Dowel.jpg

#. Attach the 800nm Longpass Filter to the Camera using Parafilm. It
   is best to wear gloves for this step.

   .. figure:: Figures/manual_hardware/standard/16_Camera_with_LP_filter.jpg
      :width: 100 %
      :alt: 16_Camera_with_LP_filter.jpg

#. Thread the camera cable from the Casing through the slit in the
   TowerBottom and through the slit of the Camera Holder.

    .. important::

        Note the orientation to avoid having to curl the camera cable
        in the camera holder

   .. figure:: Figures/manual_hardware/standard/17_Threading_of_Cam_cable.jpg
      :width: 100 %
      :alt: 17_Threading_of_Cam_cable.jpg

#. Attach the Camera Cable to the Camera in the **correct**
   orientation. Then screw the camera to the Camera Holder using the
   M2.5x10 screws. It is **not** necessary to fixate the screws with
   nuts!

   .. figure:: Figures/manual_hardware/standard/18_Camera_attached_to_CamHolder.jpg
      :width: 100 %
      :alt: 18_Camera_attached_to_CamHolder.jpg

#. Drop a 2.5mm nut in the hole in the Camera holder and use it to
   fasten the M2.5x10 screw. Then attach the CameraHolder to the
   Tower.

   .. figure:: Figures/manual_hardware/standard/19_PiVR_complete.jpg
      :width: 100 %
      :alt: 19_PiVR_complete.jpg

#. Plug the 5V power source into the micro USB slot of the Raspberry
   Pi (right side). After a couple of seconds the monitor should
   display a colorful image. Then the operating system installation
   will commence. Select the *Recommened* OS.

   .. figure:: Figures/manual_hardware/standard/20_Install_Raspian.jpg
      :width: 100 %
      :alt: 20_Install_Raspian.jpg

#. On the first startup the OS asks a couple of questions. The
   most important one is the language - make sure you choose the
   correct Keyboard layout. Make sure the Raspberry Pi is
   connected to the internet and download the
   :download:`PiVR installation file <../install_PiVR.sh>`

#. Open the terminal. Then change directory to the 'Downloads' folder
   (or wherever you downloaded the file) and type::

      bash install_PiVR.sh

   .. figure:: Figures/manual_hardware/standard/21_install_PiVR_software.jpg
      :width: 100 %
      :alt: 21_install_PiVR_software.jpg

#. While the PiVR software is being installed on the Raspberry Pi (it
   will take > 10 minutes) the light pad can be built. Take the
   diffuser. If you have access to a lasercutter, you can use
   the *svg* file in 'Hardware\Lasercutter_Files' to prepare it.
   Alternatively, you can use a drill with a M8 or M8.5 drill bit to
   drill 4 holes as shown below.

   .. figure:: Figures/manual_hardware/high_power/12_diffuser.jpg
      :width: 100 %
      :alt: 12_diffuser.jpg

#. Put in the M8 screws with a nut below and above the diffuser as
   shown below.

   .. figure:: Figures/manual_hardware/high_power/12_1_diffuser_screw_example.jpg
      :width: 100 %
      :alt: 12_1_diffuser_screw_example.jpg

#. Next, take the Aluminum heat sink and use a Sharpie and a scale to
   draw a grid with 1cm squares on the heat sink.

   .. figure:: Figures/manual_hardware/high_power/12_2_LED_heat_sink.jpg
      :width: 100 %
      :alt: 12_2_LED_heat_sink.jpg

#. Take the thermally conductive double sided tape. I usually first
   attach it to the LED before sticking them on the aluminum heat
   sink.

   .. Note::

      While you can chose LEDs of any wavelength, there is only space
      (and power) for up to 9 LEDs. In the interest of homogeneous
      illumination only a single wavelength can be used per light pad.

   Do make sure that the LEDs are space 5 cm apart using the
   previously drawn grid.

   .. figure:: Figures/manual_hardware/high_power/12_3_LEDs_on_heat_sink.jpg
      :width: 100 %
      :alt: 12_3_LEDs_on_heat_sink.jpg

#. Now take the 850 nm LED strip for background illumination and
   place it in between the high powered LED lights.

   .. figure:: Figures/manual_hardware/high_power/12_4_background_LEDs.jpg
      :width: 100 %
      :alt: 12_4_background_LEDs.jpg

#. Now, for the first row of LEDs, do the following: connect the (+) of
   the LED on the right with the (-) with the LED on its left.

   .. note::

      As heat transfers very quickly from the LED to the aluminum
      board it can be hard to liquify the solder on the LEDs. If you
      run into this problem, put some solder on the wire as well
      and keep it liquid until touching the contact on the LED.
      (Thank you Ruud Schilder for this suggestion)

   .. figure:: Figures/manual_hardware/high_power/12_6_LED_connected.jpg
      :width: 100 %
      :alt: 12_6_LED_connected.jpg

#. For the LED on the right, keep a longer, wire from (-). For the LED
   on the left, keep a longer wire from the (+).

   .. figure:: Figures/manual_hardware/high_power/12_7_one_row_done.jpg
      :width: 100 %
      :alt: 12_7_one_row_done.jpg

#. Take one of the smaller heat shrinking tubes and put it over the
   wire coming from the (+) of the LED. Take a female barrel plug and
   solder the red wire with the wire coming from the (+) of the LED
   row. When the solder has cooled down, push the heat shrink wire
   over the just soldered wire and heat it for stability of the cable.

   .. figure:: Figures/manual_hardware/high_power/12_8_connect_positive.jpg
      :width: 100 %
      :alt: 12_8_connect_positive.jpg

#. Repeat this for the wire coming from the (-) of the LED and
   connect it to the black wire of the barrel plug.

   .. figure:: Figures/manual_hardware/high_power/12_9_row_done.jpg
      :width: 100 %
      :alt: 12_9_row_done.jpg

#. Now is a good time to test whether everything works as expected so
   far. First, head over to your PiVR setup. The installation should
   have finished by now. Before plugging in the power supply you need
   to change a setting in the PiVR software:

   Go to "Options->High Power LEDs".

   In the popup window select "High Powered LED configuration" and hit
   "Exit and save changes".

   To restart the PiVR software, go to "File->Save and Exit".

   .. figure:: Figures/manual_hardware/high_power/13_PiVR_HP_settings.jpg
      :width: 100 %
      :alt: 13_PiVR_HP_settings.jpg

#. You also have to manually adapt the Pulse Width Modulation (PWM)
   frequency to the manufacturers recommendation. As you are using a
   MiniPuck, the
   `manual <https://www.ledsupply.com/content/pdf/MiniPuck_F004.pdf>`__
   states that the PWM frequency must be between 100-2kHz. See the
   :ref:`software manual<DefineGPIOsLabel>` on how to modify the PWM
   frequency.

#. Next, you will have to prepare a cable as described
   :ref:`here<detailed_soldering_instructions_cable>`.

#. Connect an appropriate power supply (make sure it is 12V and has
   enough ampere for your particular setup) to the wall and to the
   PiVR setup.

   In this particular example we are using 3 MiniPuck that have an
   output current of 700mA. The 850nm IR LEDs are rated at 24W/5M=2A of
   which we are using 3 x 20cm which is 2A x 0.06 = 120mA. The power
   supply needs to provide at least 3 x 700mA + 120mA = 2220mA. It is
   recommended to never be to close to the limit, hence I chose a 5A
   wall adapter.

   .. figure:: Figures/manual_hardware/high_power/14_power_supply.jpg
      :width: 100 %
      :alt: 14_power_supply.jpg

#. Connect the cable to the *second small connector from the top*.
   The large barrel connector needs to be connected to the female
   barrel plug you previously soldered to the LEDs.

   .. Warning::

        Important: Only connect PiVR with the LED after changing the
        settings in "Options->High Power LEDs" to "High Powered LED
        configuration". Otherwise the (very bright) LEDs will be
        completely turned on immediately.

   .. figure:: Figures/manual_hardware/high_power/15_connect_Channel_1.jpg
      :width: 100 %
      :alt: 15_connect_Channel_1.jpg

#. To test the LED, go to "Options->Optimize Image" and use the
   slider below "Channel 1". You should see the LED light up. If it
   does not, see :ref:`here<troubleshooting_LEDs>` for some
   possible problems that might cause the failure.

   .. figure:: Figures/manual_hardware/high_power/16_testing.jpg
      :width: 100 %
      :alt: 16_testing.jpg

#. After confirming that the first row of LEDs works as expected,
   continue by soldering the other rows up exactly in the same
   fashion. You will quickly notice that there are too many cables
   around. I find that using super  glue to organize the wires really
   does help. I recommend using gloves while handling super glue.

   Do make sure to keep the edges of the aluminum heat plate free as
   indicated with the red crosses below.

   It's a good idea to test each row individually for functionality
   to quickly detect soldering errors.

   .. figure:: Figures/manual_hardware/high_power/17_superglueing.jpg
      :width: 100 %
      :alt: 17_superglueing.jpg

#. Finally, connect the 850nm infrared LED strips. From each row
   connect one of the (+) with the (+) on the next row and the repeat
   for the (-). Connect the red wire of the barrel plug with the (+)
   of the LED strip.

   .. figure:: Figures/manual_hardware/high_power/18_850nm_LED_strip.jpg
      :width: 100 %
      :alt: 18_850nm_LED_strip.jpg

#. To test whether these LEDs work, take one of your cables and
   connect it to the top plug as shown below. Since these are infrared
   LEDs you will not be able to see with the bare eye, you need to use
   the PiVR camera. Then, go to "Options->Optimize Image" and drag the
   slider below 'Background' to the right. The LEDs should light up
   in the PiVR camera as shown in the image below.

   .. figure:: Figures/manual_hardware/high_power/19_850nm_LED_test.jpg
      :width: 100 %
      :alt: 19_850nm_LED_test.jpg

#. Now you are ready to test the complete setup: Make sure that
   the cable powering the infrared LEDs is connected to the top
   outlet at PiVR (top arrow).

   The high-power LEDs can be connect in any order to the 3 remaining
   outlets (the three bottom arrows).

   The diffuser should now be placed on top of the aluminum heat sink.

   .. figure:: Figures/manual_hardware/high_power/20_example_working.jpg
      :width: 100 %
      :alt: 20_example_working.jpg

#. If these tests have been successful, congratulations, you've built
   your own high-power PiVR.

.. _detailed_soldering_instructions_HP:

Detailed PCB soldering instructions (High Power Version)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. Warning::

   Important! Make sure that the pins are not connected due to
   imprecise soldering!

#. I prefer to solder the components on the PCB board in this
   particular sequence as I find it easiest to keep the components in
   place. Otherwise there is no reason to not solder components in
   any sequence you prefer!

#. To solder the PCB board you will need the following elements:

    #. One (1) Transistor: 30N06L

       .. figure:: Figures/manual_hardware/high_power/2_3_transistor_HP.jpg
          :width: 100 %
          :alt: 2_3_transistor_HP.jpg

    #. Three (3) LED Drivers "MiniPuck"

       .. figure:: Figures/manual_hardware/high_power/2_4_LEDDrivers_HP.jpg
          :width: 100 %
          :alt: 2_4_LEDDrivers_HP.jpg

    #. One (1) GPIO Header

       .. figure:: Figures/manual_hardware/standard/2_2_GPIO-Header.jpg
          :width: 100 %
          :alt: 2_2_GPIO-Header.jpg

    #. One (1) Barrel connector, 5.5mm Jack with 2.1mm center pole
       diameter

       .. figure:: Figures/manual_hardware/standard/2_3_Jack5_5.jpg
          :width: 100 %
          :alt: 2_3_Jack5_5.jpg

    #. Four (4) Barrel connectors, 3.5mm Jack with 1.35mm center pole

       .. figure:: Figures/manual_hardware/standard/2_4_Jack3_5.jpg
          :width: 100 %
          :alt: 2_4_Jack3_5.jpg

    #. One (1) :math:`10k{\Omega}` resistors

       .. figure:: Figures/manual_hardware/high_power/2_5_Resistance.jpg
          :width: 100 %
          :alt: 2_5_Resistance.jpg

    #. Break off a 5 pin stretch from the Breakaway Headers. This can
       be done using scissors or pliers.

       .. figure:: Figures/manual_hardware/standard/2_6_Headers.jpg
          :width: 100 %
          :alt: 2_6_Headers.jpg

#. Take one of  the small barrel plug and place it into the
   leftmost possible spot on the PCB board as shown.

   .. figure:: Figures/manual_hardware/high_power/S_1.jpg
      :width: 100%
      :alt: S_1.jpg

#. Flip the PCB board while holding the small barrel plug in
   place. By placing it on the table, it should not move and allow
   you to easily solder the three pins of the barrel plug to the PCB
   as shown.

   .. figure:: Figures/manual_hardware/high_power/S_2.jpg
      :width: 100%
      :alt: S_2.jpg

#. Continue to solder the other three small barrel plugs, one by one,
   onto the the PCB board.

   .. figure:: Figures/manual_hardware/high_power/S_3.jpg
      :width: 100%
      :alt: S_3.jpg

#. Next, place the GPIO header in **exactly** the orientation shown in
   the image below onto the PCB board.

   .. figure:: Figures/manual_hardware/high_power/S_4.jpg
      :width: 100%
      :alt: S_4.jpg

#. Flip the PCB board with the GPIO header around. As it now stands
   on the table it should be easy to solder. **Be
   sure the solder between the pins does not touch**

   .. figure:: Figures/manual_hardware/high_power/S_5.jpg
      :width: 100%
      :alt: S_5.jpg

#. Place the 5-pin stretch of breakaway headers into the holes on
   the far right on the PCB. Make sure to place them in the correct
   orientation as shown in the picture (short side on the 'back' of
   the PCB).

   .. figure:: Figures/manual_hardware/standard/S_6.jpg
      :width: 100%
      :alt: S_6.jpg

#. Flip the PCB with the 5-pin stretch of breakaway headers around
   and solder the header to the PCB board.

   .. figure:: Figures/manual_hardware/high_power/S_7.jpg
      :width: 100%
      :alt: S_7.jpg

#. Now take the large barrel connector and place it on the PCB at the
   indicated position

   .. figure:: Figures/manual_hardware/high_power/S_8.jpg
      :width: 100%
      :alt: S_8.jpg

#. Flip the PCB board around and solder the large barrel connector to
   the PCB board.

   .. figure:: Figures/manual_hardware/high_power/S_9.jpg
      :width: 100%
      :alt: S_9.jpg

#. Take the MiniPuck LED drivers and put them into the labelled position
   on the PCB.

   .. figure:: Figures/manual_hardware/high_power/S_10.jpg
      :width: 100%
      :alt: S_10.jpg

#. Turn the PCB around and solder each MiniPuck LED drivers to the PCB.

   .. figure:: Figures/manual_hardware/high_power/S_11.jpg
      :width: 100%
      :alt: S_11.jpg

#. After soldering the MiniPuck LED drivers, flip the PCB again. You
   will notice that they have relatively long pins (top part of image).
   You need to remove them using a wire cutter.

   .. figure:: Figures/manual_hardware/high_power/S_12.jpg
      :width: 100%
      :alt: S_12.jpg

#. Next, the :math:`10k{\Omega}` resistor will be soldered to the PCB.

   .. figure:: Figures/manual_hardware/high_power/S_13.jpg
      :width: 100%
      :alt: S_13.jpg

#. Flip the PCB board around. If the resistor falls out, just fixate
   it by bending the wire as indicated here. Then solder it the the
   PCB board.

   .. figure:: Figures/manual_hardware/high_power/S_14.jpg
      :width: 100%
      :alt: S_14.jpg

#. Similar to the MiniPuck LED drivers above, you need to remove the
   leftover wire from the resistor after soldering it to the PCB.

   .. figure:: Figures/manual_hardware/high_power/S_15.jpg
      :width: 100%
      :alt: S_15.jpg

#. Next, take transistor and place it **exactly** as
   shown onto the PCB board.

   .. figure:: Figures/manual_hardware/high_power/S_16.jpg
      :width: 100%
      :alt: S_16.jpg

#. Flip the PCB board around and solder the transistor to the board.
   Make sure the solder of the different pins does not touch the
   contact of one of the other pins! **Warning: Transistors are more
   heat sensitive compared to the other components you have used so
   far. Make sure to not let them heat up too much!**

   .. figure:: Figures/manual_hardware/high_power/S_17.jpg
      :width: 100%
      :alt: S_17.jpg

#. You must get rid of the elongated wiring of the transistors
   as not doing so will make it physically be very hard to put
   the PCB board into the casing. While it is probably best to use
   the shown wire clipper, it is also possible to do that using
   normal scissor.

   .. figure:: Figures/manual_hardware/high_power/S_18.jpg
      :width: 100%
      :alt: S_18.jpg

#. Congratulations - You have finished soldering your very own PCB
   board to build the high-powered version of PiVR!

.. _LED_measurements:

Testing homogeneity of the stimulation light
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The principle of creating virtual realities depends on the homogeneity
of the stimulation light. Below I will lay out how to measure the
light intensity and strategies on how to handle inhomogeneity.

You can use any light/power meter. We have used the PM100D with the
S130VC sensor (both from Thorlabs).

.. figure:: Figures/manual_hardware/homogeneity/1_tools.jpg
   :width: 100%
   :alt: 1_tools.jpg

Draw a grid on the diffuser. I like to use numbers for rows and the
alphabet for columns for clear separation while writing down the
light intensity.

Wear eye protection and **set the LED to 100%**.

.. Warning::

   If you are using a unusual wavelength, e.g. deep blue at ~400nm or
   below you must also protect your skin! Please consult the LED
   manufacturer safety recommendation sheet.

.. Note::

   Why not using a low light intensity, e.g. 5%? PiVR is using Pulse
   Width Modulation (PWM) to control light intensity. The light meter
   is integrating the number of photons it reads over some time t
   (which can be hard to find in the light meter manual). PWM turns
   the LED on and off rapidly (as defined
   :ref:`here<DefineGPIOsLabel>`). The easiest workaround is to just
   measure at 100% light intensity. Since PWM is guaranteed to scale
   linearly this will lead to accurate predictions of light
   intensities at e.g. 10% power.

Measure the light intensity for each square in the grid and note the
light intensity. This will give you a good idea of the light
intensity provided by the light pad and the homogeneity.

.. figure:: Figures/manual_hardware/homogeneity/2_measurement_example.jpg
   :width: 100%
   :alt: 2_measurement_example.jpg

As you can quickly see, the light intensity is not perfectly
homogeneous.

.. figure:: Figures/manual_hardware/homogeneity/3_Single_diffuser.jpg
   :width: 100%
   :alt: 3_Single_diffuser.jpg

There are several ways to optimize for homogeneity: The easiest
option is to add another diffuser plate. This will usually improve
homogeneity but knock off quite a bit of the light intensity. If you
compare the light intensity you can see that peak light intensity
decreased from :math:`40{\mu}W/mm^2` to :math:`10{\mu}W/mm^2`

.. figure:: Figures/manual_hardware/homogeneity/4_double_diffuser.jpg
   :width: 100%
   :alt: 4_double_diffuser.jpg

Finally, the reason why the light intensity falls of at the edges is
due to the fact that quite a bit of light can escape by the sides
instead of going through the diffuser. To counter this, the a
reflective border can be used - just cardboard and aluminum foil.

.. figure:: Figures/manual_hardware/homogeneity/5_double_diffuser_border.jpg
   :width: 100%
   :alt: 5_double_diffuser_border.jpg

Conclusion: While the homogeneity in the center (indicated with a
circle of diameter 90mm) is reasonable, it falls off at the edges. By
trying different heights of the second diffuser plate and by
introducing a border you can try to mitigate this.

There is an obvious software solution to this problem: For each
plate, do these measurements save on the PiVR setup. When creating
the virtual arena, tie stimulus intensity could take these
measurments into account and normalize to the maximum intensity. This
might be implemented in the future. Please check the gitlab
repository for any open issues if you need this functionality.

Additional Information
----------------------

.. _detailed_soldering_instructions_cable:

Detailed soldering instructions (cable)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. To construct 4 cables that connects PiVR with the light pad you
   will need the following components:

   .. figure:: Figures/manual_hardware/cable/1_overview.jpg
      :width: 100%
      :alt: 1_overview.jpg

   #. For each cable you will need one large barrel plug,

      .. figure:: Figures/manual_hardware/cable/1_3_large_barrel_connectors.jpg
        :width: 100%
        :alt: 1_3_large_barrel_connectors.jpg

   #. one small barrel plug, consisting of the plug (on the right side) and the holder (left side),

      .. figure:: Figures/manual_hardware/cable/1_2_small_barrel_connectors.jpg
        :width: 100%
        :alt: 1_2_small_barrel_connectors.jpg

   #. one large and two smaller heat shrinking tubes,

      .. figure:: Figures/manual_hardware/cable/1_4_heat_shrink_tubing.jpg
        :width: 100%
        :alt: 1_4_heat_shrink_tubing.jpg

   #. and some wire:

      .. figure:: Figures/manual_hardware/cable/1_5_wire.jpg
        :width: 100%
        :alt: 1_5_wire.jpg

#. Cut ~50 cm (or however long you want the wire to be). Take the
   3.5mm barrel connector and place the red wire into the center hole
   of the connector. Place the black wire into the hole on the side
   of the connector.

   .. figure:: Figures/manual_hardware/cable/2_setting_it_up.jpg
      :width: 100%
      :alt: 2_setting_it_up.jpg

#. Solder the red wire to the 3.5mm barrel connector. If there is a
   lot of leftover wire as in the image below, it is a good idea to
   cut it away.

   .. figure:: Figures/manual_hardware/cable/3_solder_pos_cable.jpg
      :width: 100%
      :alt: 3_solder_pos_cable.jpg

#. Now solder the black wire to the 3.5mm barrel connector. Again,
   make sure to remove leftover wire.

   Finally, thread the cylindrical black plastic screw on top of the
   just soldered 3.5mm barrel connector.

   .. figure:: Figures/manual_hardware/cable/4_solder_neg.jpg
      :width: 100%
      :alt: 4_solder_neg.jpg

#. After screwing the back onto the barrel connector, it should look
   like this:

   .. figure:: Figures/manual_hardware/cable/5_small_side_finished.jpg
      :width: 100%
      :alt: 5_small_side_finished.jpg

#. On the opposite side of the cable, start by placing the heat
   shrink tubes as indicated below: One small set of tubes on each of
   the thinner wires (left arrows) and one larger one on the male
   large barrel plug (right arrow).

   .. figure:: Figures/manual_hardware/cable/6_heat_shrink_tubes.jpg
      :width: 100%
      :alt: 6_heat_shrink_tubes.jpg

#. Solder the red wire of the large barrel connector to the (+) wire.
   In our example that would be red wire.

   .. figure:: Figures/manual_hardware/cable/7_large_side_soldered.jpg
      :width: 100%
      :alt: 7_large_side_soldered.jpg

#. Using a heat source, use the heat shrink tubing to stablize the
   cable: Start with the thinner wires and finally push the
   larger heat shrink tube over the other two.

   .. figure:: Figures/manual_hardware/cable/8_shrinking_the_tubes.jpg
      :width: 100%
      :alt: 8_shrinking_the_tubes.jpg

.. _troubleshooting_LEDs:

Troubleshooting LEDs
^^^^^^^^^^^^^^^^^^^^

If you can not turn the LED on using PiVR, the first test should be
whether there is an error in soldering the LEDs. To test this, plug
the 12V wall adaptor directly into the LEDs on the illumination plate.

If the LEDs do *not* turn on, it is very likely you have a loose
connection on the illumination pad. Carefully check each solder patch
. It is also possible you mixed up (+) and (-) somehwere. Finally, it
is possible that you wall adaptor is broken, however, this never
happened to me.

If the LEDs do turn on when you connect the 12V wall adaptor directly
to the LED, there are several options what could be going wrong: (1)
The cable might have a faulty connection. (2) The PCB board might
have a faulty soldering connection.

To address (1), you can use the 'resistance' option on your voltmeter
to check if the two ends of the cable are electrically connected. If
you read '0' (or a very small number), the cable is good.

To address (2), you will have to take out the PCB of the PiVR setup
again. Carefully inspect the soldering. Make sure everything that can
be soldered is soldered and that no two solder patches touch each
other. If that checks out good, double check if you soldered the GPIO
header onto the board the correct way.

If you are still not getting any light out of the LEDs, feel free to
submit at ticket on
`the PiVR repository <https://gitlab.com/LouisLab/pivr/-/issues>`__

