PiVR has been developed by David Tadres and Matthieu Louis (`Louis Lab
<https://labs.mcdb.ucsb.edu/louis/matthieu/>`__).

.. _PiVR Software Manual:

PiVR Software Manual
********************

.. warning::

    If you have the High LED power version of PiVR you **must** take
    care to properly shield yourself and others from the potentially
    very strong LED light to protect eyes and skin!

.. important::

    Several options will open a pop-up. You must close the pop-up in
    order to interact with the main window.

.. important::

    The software has different functionality if run on a Raspberry Pi
    as compared to any other PC. This software manual is for the
    Raspberry Pi version of the software


The Menubar
===========

To select a different window use the Menu Bar at the top of the window

.. figure:: Figures/manual_software/Menubar.png
   :width: 100 %
   :alt: Menubar.png

The Recording Menu
==================

The Recording Menu lets you choose between different recording
options. There are currently 4 different methods:

    1)	Tracking – Online tracking of a single animal. Possibility of
        delivering a time dependent stimulus.
    2)	VR Arena – Online tracking of a single animal. Present a
        virtual arena that will define how the stimulus is present in
        response to the position of the animal.
    3)  Dynamica VR Arena - Online tracking of a single animal. Present
        a virtual arena as above but which changes over time.
    4)	Full Frame Recording – Record an image sequence. Possibility
        of delivering a time dependent stimulus.
    5)  Timelapse Recording - Record a long image sequence at low
        frequency.
    6)	Video – Record a video (h264 format). Possibility of
        delivering a time dependent stimulus.

.. figure:: Figures/manual_software/RecordingMenu.png
   :width: 100 %
   :alt: RecordingMenu.png

Camera Control Frame
--------------------

In all of the recording options you have access to the Camera control
frame. It can be used to turn the camera preview on (Cam On) and off
(Cam Off). You can also control the size of the preview Window size.

.. warning::

    The Camera preview is always on top of everything else of the
    screen. Use the Preview Window carefully!

.. figure:: Figures/manual_software/CameraControlFrame.png
   :width: 75 %
   :alt: CameraControlFrame.png

.. _TrackingLabel:

Experiment Control Frame – Tracking
-----------------------------------

The ‘Recording’ Option you choose is printed in Bold on top of the
Experiment Control Frame. In this example it is ‘Online Tracking’.

.. figure:: Figures/manual_software/ExperimentControlFrameTracking.png
   :width: 100 %
   :alt: ExperimentControlFrameTracking.png

Online tracking tracks a **single** animal.

You have to select a folder in which the experiment will be saved by
clicking on the button to the right of ‘Save in:’

You can then give your experiment an identifier. Examples include
genotypes or an experimental treatment. This information will be
saved in your experiment folder.

If you want to present a Time Dependent Stimulus you can press the
Button ‘Select Time Dependent Stim File’. Please make sure you follow
the :ref:`guidelines <PrepTimeStimLabel>` to learn how to prepare
the file.

The figure below gives you a quick overview over the parameters used
by the program:

#. Pixel/mm: **Essential**: This value has to be set by you
   before you run your first experiment! See :ref:`set
   Pixel/mm <PixMMLabel>`. You must change it after changing
   resolution or adjusting height of the camera relative to the
   arena!

#. Framerate: The framerate you will be tracking the animal. See
   :ref:`adjust image <AdjustImageLabel>` to see how to adjust
   the framerate.

   .. warning::

      There is a difference between the framerate the camera
      can deliver and the framerate the Raspberry Pi can handle. If
      you select a very high framerate you might get a
      lower framerate than expected. Always check the
      timestamps in the ‘data.csv’ if you are trying a new,
      higher framerate than before!

#. VR stim at: N/A
#. Animal Detection Mode: Either Mode 1, Mode 2 or Mode 3. See
   :ref:`Select Animal Detection Mod <AnimalDetectionLabel>`.
#. Cam Resolution: Indicates the resolution you selected. See
   :ref:`adjust image <AdjustImageLabel>` to see how to change
   the resolution.

   .. important::

      :ref:`Online Tracking <TrackingLabel>` has only been tested
      with the following resolutions: 640x480, 1024x768, 1296x972.

#. Animal: **Essential**: for :ref:`Online Tracking
   <TrackingLabel>`. See :ref:`here <SelectOrganismLabel>` for
   how to select an animal. See :ref:`Define new
   animal<define_new_animal_label>` in case you are working with an
   animal which is not listed. If you are having problems detecting
   your animal see :ref:`here <debug_tracking>`

Next, please enter the time you want to track the animal in the field
below ‘Recording Time[s]’. Then hit ‘Start Tracking’

.. _VRLabel:

Experiment Control Frame – VR Arena
---------------------------------------------
The ‘Recording’ Option you choose is printed in Bold on top of the
Experiment Control Frame. In this example it is ‘Closed Loop
Stimulation’.

.. figure:: Figures/manual_software/ExperimentControlFrameVRArena.png
   :width: 100 %
   :alt: ExperimentControlFrameVRArena.png

Closed Loop Stimulation tracks a **single** animal.

You have to select a folder in which the experiment will be saved by
clicking on the button to the right of ‘Save in:’

You can then give your experiment an identifier. Examples include
genotypes or an experimental treatment. This information will be
saved in your experiment folder.

To present a virtual arena (stimulation depending on the position of
the animal) press the ‘Select VR Arena’ Button and select an arena.
Static virtual arenas are csv files. Note that you can present the
virtual arena either at a fixed position and independent of the
starting position of the animal (e.g. file "640x480_checkerboard.csv"
**or** you can have the position of the arena defined by the starting
position of the animal (e.g. file
"640x480_gaussian_centred_animal_pos[250, 240,0.0].csv"). See
:ref:`here <create_VR_arena>` for an in-depth explanation.

To learn how to create a new arena please see
:ref:`Create new VR Arena <create_VR_arena>`.

The box below gives you a quick overview over the parameters used by
the program:

#. Pixel/mm: **Essential**: This value has to be set by you
   before you run your first experiment! See :ref:`set Pixel/mm
   <PixMMLabel>`. You must change it after changing resolution
   or adjusting height of the camera relative to the arena!
#. Framerate: The framerate you will be using to track the
   animal. See :ref:`adjust image <AdjustImageLabel>` to see how
   to adjust framerate.

   .. warning::

       There is a difference between the framerate the camera can
       deliver and the framerate the Raspberry Pi can handle. If you
       select a very high framerate you might get a lower framerate
       than expected. Always check the timestamps in the ‘data.csv’
       if you are trying a new, higher framerate than before!

#. VR stim at: Either Head, Centroid, Midpoint or Tail. See
   :ref:`here<SelectBodyPart>` how to turn it on.
#. Animal Detection Mode: Either Mode 1, Mode 2 or Mode 3. See
   :ref:`Select Animal Detection Mod <AnimalDetectionLabel>`.
#. Cam Resolution: Indicates the resolution you selected.  See
   :ref:`adjust image <AdjustImageLabel>` to see how to change
   the resolution.

#. Animal: **Essential**: for :ref:`Closed Loop Experiments
   <VRLabel>`. See :ref:`here <SelectOrganismLabel>` for how to
   select an animal. See :ref:`Define new
   animal<define_new_animal_label>` in case you are working with an
   animal which is not listed. If you are having problems detecting
   your animal see :ref:`here <debug_tracking>`

Next, please enter the time you want to track the animal in the field
below ‘Recording Time[s]’. Then hit ‘Start Tracking VR’

.. _DynamicVRLabel:

Experiment Control Frame – Dynamic VR
---------------------------------------------
The ‘Recording’ Option you choose is printed in Bold on top of the
Experiment Control Frame. In this example it is ‘Dynamic VR’.

.. figure:: Figures/manual_software/ExperimentControlDynamicaVR.png
   :width: 100 %
   :alt: ExperimentControlDynamicaVR.png

Dynamic VR tracks a **single** animal.

You have to select a folder in which the experiment will be saved by
clicking on the button to the right of ‘Save in:’

You can then give your experiment an identifier. Examples include
genotypes or an experimental treatment. This information will be
saved in your experiment folder.

To present a dynamic virtual arena (stimulation depending on the
position of the animal) press the ‘Select VR Arena’ Button and select
an arena.
Dynamic virtual arenas are npy files. See
:ref:`here <create_dynamic_VR_arena>` for an in-depth explanation and
how to create them.

The box below gives you a quick overview over the parameters used by
the program:

#. Pixel/mm: **Essential**: This value has to be set by you
   before you run your first experiment! See :ref:`set Pixel/mm
   <PixMMLabel>`. You must change it after changing resolution
   or adjusting height of the camera relative to the arena!
#. Framerate: The framerate you will be using to track the
   animal. See :ref:`adjust image <AdjustImageLabel>` to see how
   to adjust framerate.

   .. warning::

       There is a difference between the framerate the camera can
       deliver and the framerate the Raspberry Pi can handle. If you
       select a very high framerate you might get a lower framerate
       than expected. Always check the timestamps in the ‘data.csv’
       if you are trying a new, higher framerate than before!

#. VR stim at: Either Head, Centroid, Midpoint or Tail. See
   :ref:`here<SelectBodyPart>` how to turn it on.
#. Animal Detection Mode: Either Mode 1, Mode 2 or Mode 3. See
   :ref:`Select Animal Detection Mod <AnimalDetectionLabel>`.
#. Cam Resolution: Indicates the resolution you selected.  See
   :ref:`adjust image <AdjustImageLabel>` to see how to change
   the resolution.

#. Animal: **Essential**: for :ref:`Closed Loop Experiments
   <VRLabel>`. See :ref:`here <SelectOrganismLabel>` for how to
   select an animal. See :ref:`Define new
   animal<define_new_animal_label>` in case you are working with an
   animal which is not listed. If you are having problems detecting
   your animal see :ref:`here <debug_tracking>`

Next, please enter the time you want to track the animal in the field
below ‘Recording Time[s]’. Then hit ‘Start Tracking, dynamic VR’

.. _FullFrameLabel:

Experiment Control Frame – Full Frame Recording
-----------------------------------------------
The ‘Recording’ Option you choose is printed in Bold on top of the
Experiment Control Frame. In this example it is ‘Image Sequence’.

.. figure:: Figures/manual_software/ExperimentControlFrameImageSequence.png
   :width: 100 %
   :alt: ExperimentControlFrameImageSequence.png

Image Sequence just records still images without tracking anything.
The advantage over video is that no compression of the image data is
done. The disadvantage is that it is limited by the time it takes the
Raspberry Pi to write the file on the SD card. If you are using a
higher quality SD card, you will be able to write at a higher the
framerate. However, it will probably always be lower than :ref:`video
<VideoLabel>`.

You have to select a folder in which the experiment will be saved by
clicking on the button to the right of ‘Save in:’

You can then give your experiment an identifier. Examples include
genotypes or an experimental treatment. This information will be
saved in your experiment folder.

If you want to present a Time Dependent Stimulus you can press the
Button ‘Select Time Dependent Stim File’. Please make sure you follow
the :ref:`guidelines <PrepTimeStimLabel>` to learn how to prepare
the file.

The box below gives you a quick overview over the parameters used by
the program:

#. Pixel/mm: This value indicates how many pixels are in one mm.
   You will need this value to be correct to calculate anything
   with distance afterwards (speed, distance to source etc.) See
   :ref:`set Pixel/mm <PixMMLabel>`. You must change it after
   changing resolution or adjusting height of the camera
   relative to the arena!
#. Framerate: The framerate at which you will be collecting images. See
   :ref:`adjust image <AdjustImageLabel>` to see how to adjust
   the framerate.

   .. warning::

      There is a difference between the framerate the camera
      can deliver and the framerate the Raspberry Pi can handle.
      If you select a very high framerate you might get a lower
      framerate than expected. Always check the timestamps in
      the ‘data.csv’ if you are trying a new, higher framerate
      than before!

#. VR stim at: N/A
#. Animal Detection Mode: NA.
#. Cam Resolution: Indicates the resolution you selected. See
   :ref:`adjust image <AdjustImageLabel>` to see how to change
   the resolution.
#. Animal: Value that will be saved in ‘experiment_settings.json’.

Select the image format you want your images to be in: jpg, png, rbg,
yuv or rgba. See `here <https://picamera.readthedocs.io/en/release-1
.10/api_camera.html#picamera.camera.PiCamera.capture>`__ for details
on the different formats.

Next, please enter the time you want to track the animal in the field
below ‘Recording Time[s]’.

Then hit ‘Start Recording Images'

.. _Timelapse Label:

Experiment Control Frame – Timelapse Recording
-----------------------------------------------
The ‘Recording’ Option you choose is printed in Bold on top of the
Experiment Control Frame. In this example it is ‘Timelapse’.

.. figure:: Figures/manual_software/ExperimentControlFrameTimelapse.png
   :width: 100 %
   :alt: ExperimentControlFrameTimelapse.png

Timelapse is similar to 'Image Sequence' (See above) in that it
records still images without tracking anything.
In contrast to 'Image Sequence' it allows the taking of pictures at
less than 2 frames per second, the minimal framerate for all other
modes.

You have to select a folder in which the experiment will be saved by
clicking on the button to the right of ‘Save in:’

You can then give your experiment an identifier. Examples include
genotypes or an experimental treatment. This information will be
saved in your experiment folder.

   .. Note::

      Please open a ticket on `gitlab <https://gitlab.com/LouisLab/pivr/-/issue>`__
      if you want to be able to present a time dependent stimulus.

The box below gives you a quick overview over the parameters used by
the program:

#. Pixel/mm: This value indicates how many pixels are in one mm.
   You will need this value to be correct to calculate anything
   with distance afterwards (speed, distance to source etc.) See
   :ref:`set Pixel/mm <PixMMLabel>`. You should change it after
   changing resolution or adjusting height of the camera
   relative to the arena!
#. Framerate: The framerate the camera is running.
#. VR stim at: N/A
#. Animal Detection Mode: NA.
#. Cam Resolution: Indicates the resolution you selected. See
   :ref:`adjust image <AdjustImageLabel>` to see how to change
   the resolution.
#. Animal: Value that will be saved in ‘experiment_settings.json’.

In `Recording Time` indicate the total time you wish to record.

In 'Time between Images' enter the time between frames.

   .. Warning::

      You must make sure that enough space remains on the Raspberry
      Pi. If you run out of space, the program will most likely throw
      an error and stop recording.

Select the image format you want your images to be in: jpg, png, rbg,
yuv or rgba. See `here <https://picamera.readthedocs.io/en/release-1
.10/api_camera.html#picamera.camera.PiCamera.capture>`__ for details
on the different formats.

Then hit ‘Start Timelapse`



.. _VideoLabel:

Experiment Control Frame – Video
--------------------------------
The ‘Recording’ Option you choose is printed in Bold on top of the
Experiment Control Frame. In this example it is ‘Video’.

.. figure:: Figures/manual_software/ExperimentControlVideo.png
   :width: 100 %
   :alt: ExperimentControlVideo.png

As the name indicates, use this option to record videos. The
advantage of this method over image sequence is it’s superior speed.
The disadvantage, especially for scientific questions, might be that
it compresses the image file in the temporal domain. See `here
<https://www.vcodex.com/an-overview-of-h264-advanced-video-coding/>`__
for an introduction and the Wikipedia page for more details.

You have to select a folder in which the experiment will be saved by
clicking on the button to the right of ‘Save in:’

You can then give your experiment an identifier. Examples include
genotypes or an experimental treatment. This information will be
saved in your experiment folder.

If you want to present a Time Dependent Stimulus you can press the
Button ‘Select Time Dependent Stim File’. Please make sure you follow
the :ref:`guidelines <PrepTimeStimLabel>` to learn how to prepare
the file.

The box below gives you a quick overview over the parameters used by
the program:

#. Pixel/mm: This value indicates how many pixels are in one mm.
   You will need this value to be correct to calculate anything
   with distance afterwards (speed, distance to source etc.) See
   :ref:`set Pixel/mm <PixMMLabel>`. You must change it after
   changing resolution or adjusting height of the camera
   relative to the arena!
#. Framerate: The framerate you will be recording the video. See
   :ref:`adjust image <AdjustImageLabel>` to see how to adjust
   the framerate.

   .. warning::

      There is a difference between the framerate the camera
      can deliver and the framerate the Raspberry Pi can handle.
      If you select a very high framerate you might get a lower
      framerate than expected. Always check the timestamps in
      the ‘data.csv’ if you are trying a new, higher framerate
      than before!

#. VR stim at: N/A
#. Animal Detection Mode: NA.
#. Cam Resolution: Indicates the resolution you selected. See
   :ref:`adjust image <AdjustImageLabel>` to see how
   to change the resolution.

   .. important::

      For :ref:`video <VideoLabel>` you cannot use 2592x1944.

#. Animal: Value that will be saved in ‘experiment_settings.json’.

Next, please enter the time you want to track the animal in the field
below ‘Recording Time[s]’. Then hit ‘Start Recording Images

.. _PrepTimeStimLabel:

Preparing a Time Dependent Stimulus File
=========================================

In your PiVR folder you can find a folder called
‘time_dependent_stim’. On a fresh install it is supposed to contain a
single file: blueprint_stim_file.csv

When you open it with, e.g. excel or your csv viewing program of
choice you'll see that there are 5 columns and many rows:

.. figure:: Figures/manual_software/TimeDepStimFile.png
   :width: 100 %
   :alt: TimeDepStimFile.png

The first Column (A) is the frame number. E.g. if you are recording at
30 frames per second the row 2-32 will define what’s going on in
that time.

The second column defines what Channel 1 is doing at a given frame. 0
means the light is completely OFF. 100 means the light is completely
ON. A number in between, e.g. 50 means that the light is on at
50/100=50%

The third (Channel 2), the fourth (Channel 3) and the fifth (Channel
4) use the same principle for the other channels.

It is important to notice that the stimulation file needs to be
defined on a very low level: Frame Number. The same stimulus file
will give different stimulations depending on the framerate. Therefore:

    1)	Decide on a framerate for you experiment, as an example we’ll
        say you decide on 30fps
    2)	Decide on a length of your experiment, for example 20 seconds
    3)	Decide on the stimulation pattern, e.g. you want Channel 1 to
        be OFF for the first second and
        Channel 2 to be ON for the first second. Then you want to
        switch, Channel 1 is ON for 1 sec, Channel 2 is OFF for 1 sec
    4)	You will need to set the first 30 (framerate * length of
        stimulus) rows of Channel 1 to 0
    5)	And you will need to set the first 30 (framerate * length of
        stimulus) rows of Channel 2 to 100
    6)	As you don’t care about Channel 3 and 4 you can leave it at zero
    7)	At row # 2 (since you start at row #2 in excel) or frame #
        30 (first column) you set Channel 1 to 100 for 30 rows
        (framerate * length of stimulus) to turn it ON and Channel 2
        to 0 to turn it OFF

Notes:
    A)	If you do not define enough rows for your experiment, e.g. if
        you want to run the 20 seconds experiment at 30frames per
        second but you only define what happens during the first 15
        seconds (by only going to row 15*30=450 instead of row
        20*30=600) the last value for each channel will be
        propagated, e.g. if row 450 is set to 100 and row 451 to
        600 are not defined the value 100 will be used for the rest
        of the experiment.
    B)	If you define more rows than you need for your experiments
        only the stimulation up to the point you record are used
        (this will behave as you probably expect)

.. _PixMMLabel:

Set Pixel/mm
======================

In order to set Pixel/mm for your resolution, press the ‘Options’
Menu in the Menu Bar. Then select ‘Define Pixel/mm’

.. figure:: Figures/manual_software/OptionsDefinePxMm.png
   :width: 100 %
   :alt: OptionsDefinePxMm.png

In the popup window you will see features:

    1)	The resolution you are currently using. The defined value
        will only be valid for this resolution
    2)	The Left and right cutoff slider. By moving them you can
        measure the distance.
    3)	A slice of the image taken by the camera. You want to put
        something you can measure horizontally before the camera.
    4)	A text field to enter a length you want to measure.

.. figure:: Figures/manual_software/DistanceConfigurationOverview.png
   :width: 100 %
   :alt: DistanceConfigurationOverview.png

Below an example of an adjusted distance configuration window.
Once you are satisfied with the adjustments you’ve made hit the quit
button.

.. figure:: Figures/manual_software/DistanceConfigurationAdjusted.png
   :width: 100 %
   :alt: DistanceConfigurationAdjusted.png

.. _AdjustImageLabel:

Adjust image
============

In order to set any options related to the image, press the ‘Options’
Menu in the Menu Bar. Then select ‘Optimize Image’.

.. figure:: Figures/manual_software/OptionsOptimizeImage.png
   :width: 100 %
   :alt: OptionsOptimizeImage.png

This popup should being used to set up the image in the optimal way:

    1)	Turn the camera on (‘Cam On’) if it’s not on already
    2)	Adjust the preview size so that you can comfortably see both
        the preview and the popup.
    3)	Set the framerate as desired.
    4)	Press the ‘Update Preview Framerate’ button
    5)	Set the resolution you’d like to use for the recording.

        .. important::
            For :ref:`Online Tracking <TrackingLabel>` and
            :ref:`Closed Loop Experiments <VRLabel>` only 640x480,
            1024x764 & 1296x962 has been tested

    6)	Make sure the autoexposure button says ‘autoexp on’.
    7)	Turn the Backlight Intensity up. It is normal to only see
        something above 150’000. 400’000-500’000
        is often a good value to choose.
    8)	If you have Backlight 2 intensity on one of the GPIOs (see
        :ref:`define GPIO output channels <DefineGPIOsLabel>`) you can
        also adjust Backlight 2 intensity at this point.
    9)	To test your output channels, slide the appropriate slider to
        the right. At the beginning of any experiments these will be
        turn off again. To keep a stimulus ON for the duration of the
        experiment use the Backlight 2 intensity.

.. figure:: Figures/manual_software/OptimizeImageOverview.png
   :width: 100 %
   :alt: OptimizeImageOverview.png

.. _SetUpOptimalImageLabel:

Set up optimal image
--------------------

In order to set up optimal image parameters I usually do the following:

    #) Turn 'Cam On'
    #) Set 'autoexp on'
    #) Pull 'Backlight Intensity' slider all the way to the left
       (Image will be dark)
    #) Now pull the 'Backlight Intensity' slider to the right. As
       soon as I see an image in the camera I go another 100'000 to
       the right - this way I'm not at the lower detection limit of the camera.
    #) Then I turn 'autoexp off'
    #) Often it can improve the image if I pull the 'Backlight
       Intensity' slider a bit more to the right, effectively
       overexposing the image a bit.

.. _DefineGPIOsLabel:

Define GPIO output channels
===========================

In order to define GPIO output channels for your resolution, press
the ‘Options’ Menu in the Menu Bar. Then select ‘define GPIO output
channels’.

.. figure:: Figures/manual_software/OptionsDefineOutputChannels.png
   :width: 100 %
   :alt: OptionsDefineOutputChannels.png


.. figure:: Figures/manual_software/outputChannelSelection.png
   :width: 100 %
   :alt: outputChannelSelection.png

The images on the far left indicate which of the outputs on the left
of your setups are which GPIO (e.g. the one closest to the LED power
input is GPIO#18).

There are 4 GPIO’s that can be used to control LEDs: GPIO#18,
GPIO#17, GPIO#27 and GPIO#13. GPIO#18 and
GPIO#13 are special as they are the only ones that are capable of
using PWM frequencies 40kHz.

Channel 1 is always defined as the channel that is used for the
Virtual Arena experiments.

Channel 1, Channel 2, Channel 3 and Channel 4 can be seperately
adressed using the time dependent stimulus files.

The standard frequency values are set for the normal PiVR setup
running exclusively with LED strips:

.. list-table:: Standard values
   :header-rows: 1

   * - GPIO #
     - Output Channel
     - PWM Frequency
   * - #18
     - Background
     - 40'000 Hz
   * - #17
     - Channel 1
     - 40'000 Hz
   * - #27
     - Channel 1
     - 40'000 Hz
   * - #13
     - Channel 1
     - 40'000 Hz

If you are building the :ref:`The High Powered Version<PiVR HP Version>`
you have to modify the PWM frequency to match the values in the
`datasheet <https://www.ledsupply.com/content/pdf/MiniPuck_F004.pdf>`__:

.. list-table:: High Powered LED PiVR using MiniPuck
   :header-rows: 1

   * - GPIO #
     - Output Channel
     - PWM Frequency
   * - #18
     - Background
     - 40'000 Hz
   * - #17
     - Channel 1
     - 1'000 Hz
   * - #27
     - Channel 1
     - 1'000 Hz
   * - #13
     - Channel 1
     - 1'000 Hz

.. _DebugModeLabel:

Turn Debug Mode ON/OFF
======================

In order Turn Debug Mode On or Off press ‘Options’ Menu in the Menu
Bar. Then go on ‘Turn Debug Mode…’ and select either ‘OFF’ or ‘ON’.

.. figure:: Figures/manual_software/OptionsDebugMode.png
   :width: 100 %
   :alt: OptionsDebugMode.png

.. _AnimalDetectionLabel:

Select Animal Detection Mode
============================

In order Define the Animal Detection Method press ‘Options’ Menu in
the Menu Bar. Then press ‘Animal Detection Method’.

.. figure:: Figures/manual_software/OptionsAnimalDetectionMethods.png
   :width: 100 %
   :alt: OptionsAnimalDetectionMethods.png

When in either ‘Online Tracking’ or ‘Closed Loop Stimulation the
animal needs to be detected. There are 3 modes that can be used to
detect the animal. For most cases Mode 1 (Standard) will be fine. If
you need a clear background image consider Mode 2 or Mode 3.

.. figure:: Figures/manual_software/SelectAnimalDetection.png
   :width: 100 %
   :alt: SelectAnimalDetection.png

.. _SelectOrganismLabel:

Select Organism
===============

In order select an organism press ‘Options’ Menu in the Menu Bar.
Then go on ‘Select Animal’ and select your animal.

.. figure:: Figures/manual_software/OptionsSelectAnimal.png
   :width: 100 %
   :alt: OptionsSelectAnimal.png


.. _UpdateLabel:

Updating the software
=====================

In order to update the software on your RaspberryPi, press the 'File'
Menu in the Menu Bar. Then go on 'Update Software'.

.. note::
    Please make sure you are connected to the Internet when updating.

.. figure:: Figures/manual_software/FileUpdate.png
   :width: 100 %
   :alt: FileUpdate.png

Technicalities:

This will first update our Linux by calling::

    sudo update

Next, it will download the newest version from the `gitlab
<https://gitlab.com/louislab/pivr/>`_ repository by calling::

    git pull

.. _HighLowPowerLabel:

High/Low Power LED switch
==========================
In order to choose between High and low Power LED setups press
‘Options’ Menu in the Menu Bar. Then go on ‘High Power LEDs’.

.. figure:: Figures/manual_software/OptionsHigLowPowerLEDSwitch.png
   :width: 100 %
   :alt: OptionsHigLowPowerLEDSwitch.png

Select either Standard or High power version depending on the setup
you have.

.. _SelectBodyPart:

Select Body Part for VR stimulation
===================================

When running virtual reality experiments the cells you are interested
in could be at different places of the animal.

PiVR allows you to present the virtual reality depending on
:ref:`different body parts<OutputPointExp>` identified during
tracking.

.. figure:: Figures/manual_software/OptionsVRStimulationPoint.png
   :width: 100 %
   :alt: OptionsVRStimulationPoint.png

You may choose different body parts that are defined during tracking.

.. note::
   As the difference between *centroid* and *midpoint* is not
   straightforward, please see :ref:`here <OutputPointExp>` for an
   explanation.

#. The Head (standard) will probably make a lot of sense in many
   experiments as a lot of sensory neurons of many animals are
   located there. However be aware that the Head/Tail classification
   algorithm is not perfect and does make mistakes. There is no
   option to correct for wrong head/tail assignment during the
   experiment!

#. The Centroid is probably the most consistently correct point
   during tracking. Please see
   `here <https://scikit-image.org/docs/dev/api/skimage.measure
   .html#skimage.measure.regionprops>`_ to see how it is defined.

#. The Midpoint is similar to the centroid, but can be different in
   flexible animals such as fruit fly larvae.

#. The tail is is the final option to choose from. We have used the
   presentation of the virtual reality based on tail position as a
   control in the past.

.. figure:: Figures/manual_software/VRStimulationPoint_Menu.png
   :width: 100 %
   :alt: VRStimulationPoint_Menu.png

.. _select_animal_color:

Animal Color Selection
=======================

Depending on your experimental setup, the animal can either be *dark
on white* background due to transillumination, or *white on dark*
background due to side illumination.

The standard setting is *dark on white*. If you need to change this
setting, go to Options->

.. figure:: Figures/manual_software/OptionMenuAnimalColor.png
   :width: 100 %
   :alt: OptionMenuAnimalColor.png

Now just press the button above the image that describes your
experiment.

.. figure:: Figures/manual_software/AnimalColorOptions.png
   :width: 100 %
   :alt: AnimalColorOptions.png


































