PiVR has been developed by David Tadres and Matthieu Louis (`Louis Lab
<https://labs.mcdb.ucsb.edu/louis/matthieu/>`__).

.. _PiVR source code:

PiVR software documentation
***************************

- :ref:`Graphical User interface<PiVR GUI software_documentation>`
- :ref:`Tracking software<PiVR software_documentation>`
- :ref:`Analysis<PiVR Analysis software_documentation>`
- :ref:`Virtual Arena drawing<VR drawing>`
- :ref:`Image Data Handling<PiVR Image data handling>`

.. _PiVR GUI software_documentation:

PiVR GUI source code
=====================

This page contains the classes used to construct the graphical user
interface (GUI).

.. autoclass:: start_GUI.PiVR
    :members:

.. autoclass:: start_GUI.DynamicVirtualRealityFrame
    :members:

.. autoclass:: start_GUI.TrackingFrame
    :members:

.. _PiVR software_documentation:

PiVR Tracking source code
=========================

Detection
---------
.. autoclass:: pre_experiment.FindAnimal
    :members:

Tracking
--------

.. autoclass:: control_file.ControlTracking
    :members:

.. autoclass:: fast_tracking.FastTrackingControl
    :members:

.. autoclass:: fast_tracking.FastTrackingVidAlg
    :members:

Detection and Tracking Helpers
------------------------------

.. autoclass:: tracking_help_classes.FindROI
    :members:

.. autoclass:: tracking_help_classes.MeanThresh
    :members:

.. autoclass:: tracking_help_classes.CallImageROI
    :members:

.. autoclass:: tracking_help_classes.CallBoundingBox
    :members:

.. autoclass:: tracking_help_classes.DescribeLargestObject
    :members:

.. autoclass:: tracking_help_classes.DrawBoundingBox
    :members:

.. autoclass:: tracking_help_classes.Save
    :members:

DefineOutputChannels
--------------------

.. autoclass:: output_channels.DefineOutputChannels
    :members:

Error Messages
--------------

.. autofunction:: tracking_help_classes.show_vr_arena_update_error

.. _VR drawing:

Virtual Arena drawing
======================

.. autoclass:: VR_drawing_board.VRArena
    :members:

.. autoclass:: VR_drawing_board.GaussianSlope
    :members:

.. autoclass:: VR_drawing_board.Step
    :members:

.. autoclass:: VR_drawing_board.PlaceAnimal
    :members:

.. _PiVR Analysis software_documentation:

PiVR Analysis source code
=========================

.. autoclass:: analysis_scripts.AnalysisDistanceToSource
    :members:

.. autoclass:: analysis_scripts.AnalysisVRDistanceToSource

.. autoclass:: multi_animal_tracking.MultiAnimalTracking
    :members:

.. _PiVR Image data handling:

PiVR Image Data Handling source code
====================================

.. autoclass:: image_data_handling.PackingImages
    :members:

.. autoclass:: image_data_handling.ConvertH264
    :members:

