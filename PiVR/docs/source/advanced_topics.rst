PiVR has been developed by David Tadres and Matthieu Louis (`Louis Lab
<https://labs.mcdb.ucsb.edu/louis/matthieu/>`__).

.. _advanced_topics:

Advanced topics
***************

.. _debug_tracking:

Simulate Real-Time Tracking
===========================

Imagine setting up your experiment: preparing the animals, booking
the setup/room for a whole afternoon...and then the tracker does not
track the animal half of the time!

It is quite frustrating sitting in a dark room trying to troubleshoot
these kind of problems.

To alleviate situations like these, there is the option to *simulate*
real time tracking after installing :ref:`the PiVR software on a
PC <software_installation_PC>` (=not on the Raspberry Pi).

#. At the PiVR setup, double check that:

   #. you have :ref:`set the resolution <AdjustImageLabel>` to
      640x480,
   #. that the :ref:`pixel/mm <PixMMLabel>` is set correctly,
   #. that the :ref:`framerate <AdjustImageLabel>` is identical to
      the framerate you are trying to achieve with real-time tracking.
   #. that you have :ref:`selected the correct animal <SelectOrganismLabel>`

#. Then, record a :ref:`video<VideoLabel>` with these settings. Then
   record some more.

#. Transfer the video data to your PC where you have installed the
   PiVR software and select the Debug->Simulate Online Tracking

   .. figure:: Figures/advanced_options/11_simulate_online.png
     :alt: 11_simulate_online.png
     :width: 100 %

#. Make sure the :ref:`Animal Detection Method
   <AnimalDetectionLabel>` is the same as the one you want to use
   during Real-Time tracking.

   .. note::
      This has not been tested with Mode 2

#. Select a single folder. You will now see the metadata created
   while the video was taken. Carefully inspect it to see if the
   settings are as you expect them.

   .. figure:: Figures/advanced_options/12_read_metadata.png
     :alt: 12_read_metadata.png
     :width: 100 %

#. Press the 'Track Single Animal' button - you will get a popup as
   soon as the animal detection algorithm detects a moving object.

   .. figure:: Figures/advanced_options/13_simulate_tracking_detection.png
     :alt: 13_simulate_tracking_detection.png
     :width: 100 %

#. After pressing ok, you will see what the animal detection
   algorithm has defined as the animal.

   It is obvious something has gone wrong here as the image on the
   right (the binary image) has a lot of spots where the image is
   white (=areas which are considered to be the animal)

   .. figure:: Figures/advanced_options/14_simulate_tracking_detection_broken.png
     :alt: 14_simulate_tracking_detection_broken.png
     :width: 100 %

#. After pressing Ok, the tracking algorithm starts - as the animal
   has not been properly identified in the first frame, the tracking
   algorithm is unable to identify the animal during tracking as well:

   .. figure:: Figures/advanced_options/15_simulated_tracking_broken.png
     :width: 100 %
     :alt: 15_simulated_tracking_broken.png

#. After going through the simulated tracking, the potential source
   of the problem has been identified: The animal can not be detected
   correctly. There are a many reason why this could be:

   #. The edge of the dish seems to have moved a bit during the first
      couple of frames (red rectangle). If you are able to stabilize
      the setup to ensure no movement while doing experiments, this
      problem should be solved.

      .. figure:: Figures/advanced_options/16_detection_problems1.png
         :width: 100 %
         :alt: 16_detection_problems1.png

   #. The fact that several spots in the middle of the dish are
      wrongly binarized as the potential animal, indicates that the
      detection algorithm has trouble setting the threshold correctly.
      This problem arises because the threshold is calculated as 2
      standard deviations from the mean of the pixel intensities in
      the subtracted image :func:`pre_experiment.FindAnimal.define_animal_mode_one`.
      While the animal is the darkest spot in the image, the whole
      petri dish is darker than the background which might lead to
      this problem.

      .. figure:: Figures/advanced_options/17_detection_problem2.png
         :width: 100 %
         :alt: 17_detection_problem2.png

      There are two general ways to solve this problem:

         #. Optimize the :ref:`imaging conditions<AdjustImageLabel>`
            so that the animal has a higher contrast to the
            background, which should be as homogenous as possible.
            See :ref:`here<SettingUpArenaCamera>` for an example.

         #. Optimize the animal parameters. You can follow :ref:`this
            guide <define_new_animal_label>` to set stringent animal
            parameters for tracking.

#. There are many ways how tracking can fail. Only a single
   example is described above. I hope the walkthrough will enable
   you to generally get an idea where during tracking the
   algorithm fails.

.. _define_new_animal_label:

Tracking of a new animal
========================

PiVR has been used to track a variety of animals: Walking adult fruit
flies, fruit fly larvae, spiders, fireflies, pillbugs and zebrafish.

For the tracking algorithm to function optimally, it takes several
"animal parameters" into account:

#. The amount of pixels the animal will fill in the image.

#. The "roundness" of the animal.

#. The proportions of the animal.

#. The length of the animal.

#. The speed of the animal.

For each animal you can choose in
:ref:`Options->Select Organism<SelectOrganismLabel>` these parameters
were defined. You can find them in the file
"list_of_available_organisms.json" in source code.

If you want to track an animal that is **not** on the list you can
always try to use the "Not in list" option. However, the tracking
algorithm might not work optimally.

There is a straightforward pipeline to collect the necessary animal
parameters to optimize real-time tracking:

#. Place your (**single!!**) animal in the arena you want to use for
   your experiment.

#. As always, do not forget to :ref:`define the pixel/mm <PixMMLabel>`.

#. Select "Not in List" under :ref:`Options->Select
   Organism<SelectOrganismLabel>`.

#. Record a :ref:`video<VideoLabel>`. If you use a fast animal, make
   sure to select a sufficiently high framerate. **You must use
   640x480 resolution**. As always it is imperative that the camera
   and the arena are stable during recording, i.e. nothing in the
   image should move except the animal!

#. Record for a couple of minutes, i.e. 5 minutes.

#. Make sure you have videos with animals moving as fast as they
   might in your actual experiment.

#. It is also necessary that the animals move for a large fraction of
   the video!

#. Take the videos to your PC on which you have :ref:`installed the
   PiVR software <software_installation_PC>`.

#. To observe what the algorithm is doing, turn the :ref:`Debug mode
   on<DebugModeLabel>`. This is recommend as you will see immediately
   if and where something goes wrong. This can help you to solve
   tracking problems.

#. Analyze each video using the :ref:`Tools->Analysis: Single Animal
   Tracking <tools_single_animal_tracking>` option.

#. If using the Debug mode, you will get informed as soon as the
   algorithm detects an object that is moving. It will also inform
   you how much space (in pixels) the detected animal occupies, its
   eccentricity ('roundness') and a parameter for proportions (Major
   axis over minor axis). If the identified object clearly is **not**
   the animal answer the question with "No" and the algorithm will
   look in the next frame the largest moving object.

   .. figure:: Figures/advanced_options/1_debug_detection1.png
      :alt: 1_debug_detection1.png
      :width: 100 %

#. Next, you will be shown a side by side comparison of the original
   picture (with a box drawn around the detected animal and the binary
   image you have seen in the previous popup.

   .. figure:: Figures/advanced_options/2_debug_detection2.png
      :alt: 2_debug_detection2.png
      :width: 100 %

#. The algorithm will then start tracking. You will see an overview
   of how the algorithm detects the animal: On the left you can see
   the original image. In the center you can see the binary image:
   The grey area indicates the search box (which depends on defined
   max speed of animal, pixel/mm and framerate) and in white the
   pixels that are below threshold. The black area is not considered
   as it is too fare away from the position of the animal in the
   previous frame. On the right, you can see the result of the
   tracking: A box drawn around the identified animal. In addition,
   you can see the animal parameters you are looking for. These are
   just for your information, read below to see how to comfortably
   get the list of these parameters.

   .. figure:: Figures/advanced_options/3_debug_tracking.png
      :width: 100 %
      :alt: 3_debug_tracking.png

#. After running the Single Animal Tracking algorithm, you will find
   a number of new files in each experimental folder. To get to the
   animal parameters, open the file "DATE_TIME_heuristics.csv", for
   example with excel.

   .. figure:: Figures/advanced_options/4_heuristics.png
      :alt: 4_heuristics.png
      :width: 100 %

#. Each row in the table stands for one frame. The title of the
   column describes the value.

   .. figure:: Figures/advanced_options/5_heuristics_columns.png
      :width: 100 %
      :alt: 5_heuristics_columns.png

#. You need to get the following values to get all animal parameters:

   #. A minimum value for filled area (in mm)

   #. A maximum value for filled area (in mm)

   #. A minimum value for eccentricity

   #. A maximum value for eccentricity

   #. A minimum value for major over minor axis

   #. A maximum value for major over minor axis

   #. Maximum skeleton length

   #. Maximum speed of the animal (mm/s)

#. As the tracking algorithm needs the extreme values to function
   properly, I have found it easiest to plot a Line Plot for each
   experiment for each of the relevant parameters. For example for
   the filled area:

   .. figure:: Figures/advanced_options/6_heuristic_plot_example.png
      :alt: 6_heuristic_plot_example.png
      :width: 100 %

#. Write down the maximum and minimum value for each of relevant
   parameters. In this example, the minimum value for filled area in
   mm would be ~25 and the maximum would be ~90.

#. Do the same for eccentricity, major over minor axis, skeleton
   length and maximum speed (mm/s)

#. Then do the same for a few other videos. The goal is to get
   extreme values without having to put 0 as minimum and infinity as
   maximum.

#. In this example I have found the following parameters:

   #. Minimum value for filled area (in mm): 20

   #. Maximum value for filled area (in mm): 90

   #. Minimum value for eccentricity: 0.4

   #. Maximum value for eccentricity: 1

   #. Minimum value for major over minor axis: 1

   #. Maximum value for major over minor axis: 3.5

   #. Maximum skeleton length: 14

   #. Maximum speed of the animal (mm/s): 350

#. Now go to the PiVR software folder on your PC and find the file
   named: "list_of_available_organisms.json":

   .. figure:: Figures/advanced_options/7_open_organism_json.png
      :alt: 7_open_organism_json.png
      :width: 100 %

#. Open it with an text editor. I often use "Code Writer" that ships
   with Windows. You will see that there are repeating structures: A
   word, defining the name of the animal, then a colon and then some
   image parameters in brackets.

   .. note::
      Json files require correct  `formatting <https://en.wikipedia
      .org/wiki/JSON>`__. Be careful to not accidentally deleting
      commas etc.

   .. figure:: Figures/advanced_options/8_open_organism_json.png
      :alt: 8_open_organism_json.png
      :width: 100 %

#. To enter your animal parameters you have two options: The easiest
   (and safest) option is to choose an animal in the list that you
   are certain to never use and just enter your parameters:

   .. figure:: Figures/advanced_options/9_modified_organism_json.png
      :alt: 9_modified_organism_json.png
      :width: 100 %

   Alternatively, you may also enter a new "cell" at the end of the
   list. There is no limit on the number of different animals that
   can be entered in this list.

#. Now save the file (do **not** rename it - If you want to keep a
   backup, rename the original, i.e. to
   "list_of_available_organisms_original.json".)

#. Restart the PiVR software (so that it reads the newly defined
   animal parameters).

#. If you want to know whether PiVR is able to perform real-time
   tracking, you can open the "experiment_settings.json" files in one
   of the video folders you used to find the animal parameters (or a
   newly created video) and change the "Model Organism" cell name to
   your animal name

   .. figure:: Figures/advanced_options/10_change_experiment_settings.png
      :alt: 10_change_experiment_settings.png
      :width: 100 %

#. Now, select the "Debug->Simulate Online Tracking" window, select a
   video and check :ref:`whether the algorithm can track the animal in
   real-time<debug_tracking>`. If not, you might have to select more
   stringent animal parameters and/or you have to optimize imaging
   conditions.



