
PiVR has been developed by David Tadres and Matthieu Louis (`Louis Lab
<https://labs.mcdb.ucsb.edu/louis/matthieu/>`__).

.. _BOM:

Bill of Materials (BOM)
========================

All components you need to build your

#. :ref:`Standard PiVR <BOM_std>`

#. :ref:`High Powered PiVR <BOM_HP>`

.. _BOM_std:

Standard PiVR setup
--------------------

For detailed information, scroll to the right.

This table is identical to the one found on
`Gitlab <https://gitlab.com/LouisLab/pivr/blob/master/PiVR/Hardware/
Inventory>`__ which might be more convenient when ordering parts.

.. csv-table:: Table Title
   :file: ../../Hardware/Inventory/PiVR_inventory_standard.csv
   :header-rows: 1

.. _BOM_HP:

High Powered PiVR setup
-----------------------

This table is identical to the one found on
`Gitlab <https://gitlab.com/LouisLab/pivr/blob/master/PiVR/Hardware/
Inventory>`__ which might be more convenient when ordering parts.

.. csv-table:: Table Title
   :file: ../../Hardware/Inventory/PiVR_inventory_HighPower.csv
   :header-rows: 1