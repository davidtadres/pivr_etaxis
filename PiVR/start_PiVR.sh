#!/bin/bash

# IMPORTANT: Don't open this file on Windows and save again. The reason being that line endings are different between
# DOS/Windows and Unix/Linux. DOS uses carriage return and line feed ('\r\n') while Unix uses just line feed ('\n').
# If this file is opened and saved in Windows just run this command before opening the sh file on Linux

#echo calling pigpiod
sudo pigpiod -s 1
#echo next
python3 /home/pi/pivr_etaxis/PiVR/start_GUI.py
echo DO NOT CLOSE THIS WINDOW!!!
