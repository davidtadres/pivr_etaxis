import initialize_generic_sensor

generic_sensor_object = initialize_generic_sensor.InitializeGenericSensor()
#voltmeter_object.device_voltmeter.get_currentValue()

# This might be much faster!
# print(float(generic_sensor_object.device_generic_sensor.loadAttribute('currentValue')))

def return_generic_value():
    """
    From the Yoco API:

    # Returns the current value of the current, in mA, as a floating point number
    
    Changed from 'get_currentValue' to 'loadAttribute' as this is supposed to be muuuch
    faster: https://www.yoctopuce.com/EN/article/optimizing-the-reading-of-yoctopuce-sensors
    """
    #return(generic_sensor_object.device_generic_sensor.get_currentValue())
    if generic_sensor_object.device_generic_sensor is not None:
        current_value = float(generic_sensor_object.device_generic_sensor.loadAttribute('currentValue'))
    else:
        current_value = None

    return(current_value)
