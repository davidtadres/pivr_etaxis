import yoctopuce.yocto_api as yapi
import yoctopuce.yocto_voltage as yvolt
import sys

errmsg = yapi.YRefParam()
if yapi.YAPI.RegisterHub("usb", errmsg) != yapi.YAPI.SUCCESS:
    sys.exit("init error :" + errmsg.value)
else:
    print('Connected Yocto USB device')


class InitializeVoltmeter():
    """
    This class tries to find the first Yoctopuce Voltage device
    connected through USB.

    It returns a 'device_voltmeter' object that can be used to read
    values of the voltmeter
    """
    def __init__(self):
        self.sensorDC = None
        self.sensorAD = None # Uncomment to get AC reading

        self.initialize()

    def initialize(self):
        #self.device_voltmeter = yvolt.YVoltage.FirstVoltage()
        #if self.device_voltmeter is not None:
        #    print('Sucessfully connected to a Yoctopuce Voltmeter')
        #else:
        #    print('could not connect')
        
        # retreive any voltage sensor
        sensor = yvolt.YVoltage.FirstVoltage()

        if sensor is not None:
            m = sensor.get_module()
            target = m.get_serialNumber()

            self.sensorDC = yvolt.YVoltage.FindVoltage(target + '.voltage1')
        # self.sensorAC = yvolt.YVoltage.FindVoltage(target + '.voltage2') # uncomment to get AC reading!
        
        if self.sensorDC is not None:
            print('Sucessfully connected to a Yoctopuce Voltmeter')
        else:
            print('could NOT connect to a Yoctopuce Voltmeter')
        


