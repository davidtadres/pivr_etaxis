__author__ = 'David Tadres'
__project__ = 'PiVR'

import tkinter as tk
import os

"""
Whenever an AddOn is written it should integrate itself with the PiVR
software.

Easiest (I think) to provide a python file which always has the same
name (but of course in different folders) which defines the window
with the options the user can choose from.

"""


class YoctoOptionMenu():
    '''
    In order for the program to know how many pixel in a image taken translate to a given distance the user
    needs to provide some information.
    The user needs to provide an object with a known length in mm. This objects needs to be placed in front
    of the camera. When calling this function the user has to enter the length of the object and, using
    sliders, adjust the image seen so that the edges of the image are at the same place as the edge of the
    object.
    '''

    # The class take the camera object, the current resolution and a previous value for the known distance.
    # if the user never entered the distance before it will be 1
    def __init__(self,
                 voltage_bool=None,
                 generic_bool=None):
        # take the input and define variables from them
        '''
        self.camera = cam
        self.resolution = resolution
        print(self.resolution)
        self.known_distance = known_distance

        '''
        self.y_voltage_bool = voltage_bool
        self.y_generic_bool = generic_bool

        # create a new window and name it
        self.child = tk.Toplevel()
        self.child.wm_title(
            'Yoctopuce Menu')

        #disable main window
        self.child.grab_set()

        self.child_frame = tk.Frame(self.child,
                                    #width=100,
                                    #height=50
                                    )

        self.child_frame.pack()

        self.some_info_label = tk.Label(self.child_frame,
                    text='Use Yoctopuce devices.\n'
                         'Currently only Voltage sensors and \n'
                         'generic sensors are supported.\n\n'
                         'Currently only works with video!\n\n'
                         'If you want more support, check the API\n'
                         'or open an issue on Gitlab',
                    anchor='w'
                    )
        self.some_info_label.grid(row=0, column=0)

        self.voltage_button = tk.Checkbutton(self.child_frame,
                                             text='Voltage',
                                             variable  =self.y_voltage_bool)
        self.voltage_button.grid(row=1, column=0)

        self.generic_button = tk.Checkbutton(self.child_frame,
                                             text='Generic',
                                             variable = self.y_generic_bool )
        self.generic_button.grid(row=2, column=0)
