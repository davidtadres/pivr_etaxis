import initialize_voltage

voltmeter_object = initialize_voltage.InitializeVoltmeter()
#voltmeter_object.device_voltmeter.get_currentValue()

def return_voltage():
    """
    Changed from 'get_currentValue' to 'loadAttribute' as this is supposed to be muuuch
    faster: https://www.yoctopuce.com/EN/article/optimizing-the-reading-of-yoctopuce-sensors
    """
    #return(voltmeter_object.device_voltmeter.get_currentValue())
    if voltmeter_object.sensorDC is not None:
        current_value = float(voltmeter_object.sensorDC.loadAttribute('currentValue'))
    else:
        current_value = None
    return(current_value)
