import yoctopuce.yocto_api as yapi
import yoctopuce.yocto_genericsensor as ygeneric
import sys

errmsg = yapi.YRefParam()
if yapi.YAPI.RegisterHub("usb", errmsg) != yapi.YAPI.SUCCESS:
    sys.exit("init error :" + errmsg.value)
else:
    print('Connected Yocto USB device')


class InitializeGenericSensor():
    """
    This class tries to find the first Yoctopuce Voltage device
    connected through USB.

    It returns a 'device_voltmeter' object that can be used to read
    values of the voltmeter
    """
    def __init__(self):
        self.device_generic_sensor = None

        self.initialize()

    def initialize(self):
        self.device_generic_sensor = ygeneric.YGenericSensor.FirstGenericSensor()
        if self.device_generic_sensor is not None:
            print('Sucessfully connected to a Yoctopuce Generic Sensor')
        else:
            print('could NOT connect to a Yoctopuce Generic Sensor')

