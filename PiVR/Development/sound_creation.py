'''
sounddevice seems to only exist for Linux
We can use this for users to create their own sounds
'''

import numpy as np
import sounddevice as sd
#from pydub import AudioSegment
#import pydub
#import alsaaudio
import time

#sd.default.samplerate = 44100

time_sample = 2.0
frequency = 440

# Generate time of samples between 0 and two seconds
samples = np.arange(44100 * time_sample) / 44100.0
# Recall that a sinusoidal wave of frequency f has formula w(t) = A*sin(2*pi*f*t)
wave = 10000 * np.sin(2 * np.pi * frequency * samples)
# Convert it to wav format (16 bits)
wav_wave = np.array(wave, dtype=np.int16)

sd.play(wav_wave, loop=True)

#volume_output = alsaaudio.Mixer('PCM', 0)
time.sleep(4)


