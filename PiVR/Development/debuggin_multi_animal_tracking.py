# Get a maximum distance! IF we have a mis-detection of an animal (cam noise etc)
# and two or more animals are currently crashing the algorithm will jump to the
# noise - easiest way is to implement a maximal distance the animals travel between
# frames - must be user provided as it will vary between experiments, i.e. if one
# animal is lost for a longer time > Problem - will become less stable when animal
# disappears due to bad image - maybe this is a limitation of this algorithm I'm able
# to cope with as the solution is to take clear videos

import numpy as np
import matplotlib.pyplot as plt
import os
from skimage import measure
import math

#speed = 1 # in mm/s
#px_per_mm = 2.65
#fps = 3
#max_movement_in_pixel_per_frame = speed * px_per_mm  /fps

minimal_filled_area = 15
maximal_filled_area = 100
major_over_minor = 1

max_speed = 1.5
pixel_per_mm = 2.56
max_speed_pixel = pixel_per_mm * max_speed

os.chdir('G:\\My Drive\\PhD\\Pheromone_Project\\Behavior\\David Data\\Z5'
         '\\180902 - Pheromone assay test\\B_was_1_to_1000\\02.09.2018_14-25-29_OregonR\\') # PC Home
# os.chdir('C:\\Users\\tadres\\Desktop\\11.08.2018_15-00-11_OregonR\\') # work PC
background = np.load('Background.npy')
smooth_images = np.load('smoothed_images.npy')

subtracted_images = background[:,:,np.newaxis].astype(np.int16) - smooth_images.astype(np.int16)

mean_images = np.nanmean(subtracted_images,axis=(0,1))
std_images = np.nanstd(subtracted_images, axis=(0,1))
thresholed_images = subtracted_images > mean_images + 5*std_images

image_number = 430 # for testing purposes

blobs = measure.regionprops(measure.label(thresholed_images[:,:,image_number]))

blob_counter = 0
for j_blob in range(len(blobs)):
    if minimal_filled_area < blobs[
            j_blob].filled_area < maximal_filled_area and  blobs[
            j_blob].minor_axis_length > 0 and blobs[
            j_blob].major_axis_length / blobs[
            j_blob].minor_axis_length > major_over_minor:
        print(j_blob)
        blob_counter += 1
print('Found ' + repr(blob_counter) + ' blobs with appropriate size')

# User at this point already decided that this blob_counter number is the correct number of animals
centroids = np.zeros((3,blob_counter, smooth_images.shape[2]))
centroids.fill(np.nan)
bounding_boxes = np.zeros((4,blob_counter, smooth_images.shape[2]))
bounding_boxes.fill(np.nan)

missing_animal = np.zeros((2,blob_counter))
missing_animal.fill(np.nan)

# initial position of the animals
blob_counter = 0
for j_blob in range(len(blobs)):
    if minimal_filled_area < blobs[
                    j_blob].filled_area < maximal_filled_area and blobs[
                    j_blob].major_axis_length / blobs[
                    j_blob].minor_axis_length > major_over_minor:
        print(blob_counter)
        centroids[:, blob_counter, image_number] = int(blobs[j_blob].centroid[0]), int(blobs[j_blob].centroid[1]), 1 # row, then column, number of animals at this position - must be one at this frame as this is the user provided frame
        bounding_boxes[:, blob_counter, image_number] = int(blobs[j_blob].bbox[0]), int(blobs[j_blob].bbox[1]), \
                                             int(blobs[j_blob].bbox[2]), int(blobs[j_blob].bbox[3])
        blob_counter += 1

fig=plt.figure()
ax=fig.add_subplot(111)
plot_image = ax.imshow(background,cmap='Greys_r')

plot_background = fig.canvas.copy_from_bbox(ax.bbox)
scats = []

def get_cmap(n, name='hsv'):
    """Returns a function that maps each index in 0, 1, ..., n-1 to a distinct
    RGB color; the keyword argument name must be a standard mpl colormap name."""
    return plt.cm.get_cmap(name, n)
cmap = get_cmap(centroids.shape[1]+1)

plot_image.set_data(smooth_images[:, :, image_number])
fig.canvas.restore_region(plot_background)

for scat in scats:
    scat.remove()
scats = []

for m in range(centroids.shape[1]):
    scats.append(ax.scatter(x=centroids[1, m, image_number], y=centroids[0, m, image_number], c=cmap(m)))
# ax.draw_artist(plot_background)
plt.draw()
#plt.pause(0.5)

number_of_frames_animal_lost = np.ones((blob_counter))

# for i_frame in reversed(range(image_number)):
backwards = False
for i_frame in (range(image_number+1, image_number+40)):
    print('frame ' + repr(i_frame))
    # Strongly prefer to work with bounding boxes and local thresholds and no restriction on size etc as all that
    # can easily change in an experiment. Just simple distance rule if in case

    # but...bounding boxes are quite crap in distinguishing animals. If use a general blob detection algorithm it can
    # fail during crossings where there is only one blob, but it will recover. But we will have to use heuristic rules
    # such as filled area etc for defining a blob.
    # Using a distance rule (shortest distance = same animal, thinking about it, as we have max speed of animal, fps and
    # px per mm we can also use a max distance to ensure there's no jumping around) I can define which blob is the same
    # as in the last frame. If no animal is in the vicinity just drop it and put in something clear, such as (0,0). It
    # should recover afterwards and we can just implement a very simple interpolation to get rid of these afterwards.
    # This should make for very readable code. I don't think there should be many if...else

    # detect all blobs in image
    blobs = measure.regionprops(measure.label(thresholed_images[:, :, i_frame]))
    animals_counted = 0

    minimal_dist = np.zeros((centroids.shape[1]))
    minimal_dist.fill(np.nan)
    animal_index = np.zeros((centroids.shape[1]))
    animal_index.fill(np.nan)
    blob_index = np.zeros((centroids.shape[1]))
    blob_index.fill(np.nan)

    for j_blob in range(len(blobs)):
        if minimal_filled_area < blobs[
            j_blob].filled_area < maximal_filled_area and  blobs[
            j_blob].minor_axis_length > 0 and blobs[
            j_blob].major_axis_length / blobs[
            j_blob].minor_axis_length > major_over_minor:
            animals_counted += 1

    #print('animal_counted ' + repr(animals_counted))

    previous_centroid_positions = np.zeros((centroids.shape[0], centroids.shape[1], centroids.shape[2],2))

    if backwards:
        # create array to which distance should be calculated - if none of the animals are touching each other
        # it's just a copy of centroids[:,:,i_frame+1]
        previous_centroid_positions = centroids[:2, :, i_frame + 1].copy()
        # count number of animals counted in previous frame
        animals_counted_in_previous_frame = np.nansum(centroids[2, :, i_frame + 1])
        # test if less animals than expected
        if animals_counted_in_previous_frame < centroids.shape[1]:
            # which index is was missing last frame?
            for i_missing in range(len(np.where(np.isnan(previous_centroid_positions[0]))[0])):
                # find the index of the missing animal in the last frame
                np.where(np.isnan(previous_centroid_positions[0]))[0][i_missing]
                # just add a one for each frame that the animal is missing
                number_of_frames_animal_lost[np.where(np.isnan(previous_centroid_positions[0]))[0][i_missing]] += 1
    else:
        # create array to which distance should be calculated - if none of the animals are touching each other
        # it's just a copy of centroids[:,:,i_frame+1]
        previous_centroid_positions = centroids[:2, :, i_frame - 1].copy()
        # count number of animals counted in previous frame
        animals_counted_in_previous_frame = np.nansum(centroids[2, :, i_frame - 1])
        # test if less animals than expected
        if animals_counted_in_previous_frame < centroids.shape[1]:
            # which index is was missing last frame?
            for i_missing in range(len(np.where(np.isnan(previous_centroid_positions[0]))[0])):
                # find the index of the missing animal in the last frame
                np.where(np.isnan(previous_centroid_positions[0]))[0][i_missing]
                # just add a one for each frame that the animal is missing
                number_of_frames_animal_lost[np.where(np.isnan(previous_centroid_positions[0]))[0][i_missing]] += 1

    print(number_of_frames_animal_lost)
    ###################
    # First try - turns out it can happen that in the same frame a crash is over when bad images lead to loss of a single
    # animal somewhere else in the frame - get rid of the if and always compare last position
    ######
    # if there are more animals in the current frame compared to the previous frame try to assign the newly appeared
    # animal to the last known position of the, at one point lost, animal
    # Might be more than one animal that appears in a given frame
    for m in range(len(np.argwhere(np.isnan(previous_centroid_positions[0, :])))):
        # assign to previous centroid position search array so that distance to the current centroid position can
        # be calculated in the next loop to assign non-optimal animal identity
        previous_centroid_positions[:, np.argwhere(np.isnan(previous_centroid_positions[0, :]))[0][0]] = \
            missing_animal[:, np.argwhere(np.isnan(previous_centroid_positions[0, :]))[0][0]]

    # Do nothing if in current frame are less animals compared to the frame analyzed before
    else:
        pass

    animals_counted = 0

    for j_blob in range(len(blobs)):

        # put some selection heuristics such as minimum and maximum shape etc.
        if minimal_filled_area < blobs[
            j_blob].filled_area < maximal_filled_area and  blobs[
            j_blob].minor_axis_length > 0 and blobs[
            j_blob].major_axis_length / blobs[
            j_blob].minor_axis_length > major_over_minor:
            # switch that gets turned on if animal has beend found - need one for each blob
            found_animal = False
            # plt.imshow(smooth_images[blobs[j_blob].bbox[0]:blobs[j_blob].bbox[2], blobs[j_blob].bbox[1]:blobs[j_blob].bbox[3],i_frame])
            # plt.show(block=True)
            # When a crash is happening there will be one animal less than expected! Need to count number of assigned
            # animals

            # for each blob that is accepted as an animal, calculate the distance to all blobs in previous frame - only
            # keep the minimal distance and the animal that had the minimal distance. Number of position also indicates
            # which position
            for n_animal in range(previous_centroid_positions.shape[1]):
                # Check if the animal index is already taken. If so, first come first serve, just skip that previous
                # animal position!
                if n_animal not in animal_index:
                    # for readability, explicity calculate distance here:
                    current_dist = np.linalg.norm(
                        previous_centroid_positions[:, n_animal] -
                        np.asarray((blobs[j_blob].centroid[0],
                                    blobs[j_blob].centroid[1])))
                    # as the minimal_dist array is filled with NaNs we can just check if this is the first time in this
                    # loop we are trying to assign a minimal distance to this animal
                    if np.isnan(minimal_dist[animals_counted]):
                        # only assign the minimal_dist if the minimal dist is actually realistic
                        if current_dist < max_speed_pixel * number_of_frames_animal_lost[n_animal]:
                            # calculate eucledian minimal distance
                            minimal_dist[animals_counted] = current_dist
                            # previous animal index
                            animal_index[animals_counted] = n_animal
                            # current animal index
                            blob_index[animals_counted] = j_blob
                            # only count animal once - if we update in the elif it's still only one
                            found_animal = True

                    # if this
                    else:
                        # only assign the minimal_dist if the minimal dist is actually realistic
                        if current_dist < max_speed_pixel * number_of_frames_animal_lost[n_animal]:
                            if current_dist <minimal_dist[animals_counted]:
                                # calculate eucledian minimal distance
                                minimal_dist[animals_counted] = current_dist
                                # previous animal index
                                animal_index[animals_counted] = n_animal
                                # current animal index
                                blob_index[animals_counted] = j_blob
                            found_animal = True

                    """
                    if np.isnan(minimal_dist[animals_counted]):
                        # calculate eucledian minimal distance
                        minimal_dist[animals_counted] = np.linalg.norm(previous_centroid_positions[:, n_animal] -
                                                                       np.asarray((blobs[j_blob].centroid[0],
                                                                                   blobs[j_blob].centroid[1])))
                        # previous animal index
                        animal_index[animals_counted] = n_animal
                        # current animal index
                        blob_index[animals_counted] = j_blob

                    # if minimal distance larger than distance to next previous animals
                    elif minimal_dist[animals_counted] > np.linalg.norm(previous_centroid_positions[:, n_animal] -
                                                                        np.asarray((blobs[j_blob].centroid[0],
                                                                                    blobs[j_blob].centroid[1]))):
                        # update the minimal distance to the smaller one
                        minimal_dist[animals_counted] = np.linalg.norm(previous_centroid_positions[:, n_animal] -
                                                                       np.asarray((blobs[j_blob].centroid[0],
                                                                                   blobs[j_blob].centroid[1])))
                        # and of course also the index for both the previous animal
                        animal_index[animals_counted] = n_animal
                        # and the current animal
                        blob_index[animals_counted] = j_blob
                    """
            if found_animal:
                animals_counted += 1

        # as there is an if clause that makes a blob count as an animal or not. There's a need to have a counter for
        # that updates

    #centroids[:, :, i_frame] = np.nan # todo - for debugging, delete!
    print(animal_index)
    # assign each animal to the closest previous animal - in case an animal is lost/crashed it will just stay empty as
    # no new animal is closer than to another, still existing animal.
    for n_animal_assignment in range(centroids.shape[1]):
        #print(not np.isnan(blob_index[n_animal_assignment]))
        if not np.isnan(blob_index[n_animal_assignment]):
            # print(int(animal_index[n_animal_assignment]), i_frame)
            # check if distance traveled to last known point is realistic:
            if minimal_dist[n_animal_assignment] < \
                max_speed_pixel*number_of_frames_animal_lost[n_animal_assignment] :
                # for debuggin
                if number_of_frames_animal_lost[n_animal_assignment] > 1:
                    print(max_speed_pixel*number_of_frames_animal_lost[n_animal_assignment])

                centroids[:, int(animal_index[n_animal_assignment]), i_frame] = \
                    int(blobs[int(blob_index[int(n_animal_assignment)])].centroid[0]), \
                    int(blobs[int(blob_index[int(n_animal_assignment)])].centroid[1]), 1
                if number_of_frames_animal_lost[int(animal_index[n_animal_assignment])] != 1:
                    number_of_frames_animal_lost[int(animal_index[n_animal_assignment])] = 1
            else:
                # just leave it as a nan
                pass



    # How many previously identified animals could not be assigned a blob this frame?
    if np.nansum(centroids[2, :, i_frame]):
        # for each of those:
        for m in range(len(np.argwhere(np.isnan(centroids[2, :, i_frame])))):
            # only insert the nan in case it hasn't been a nan before. Essentially we want to save the last known
            # position of the animal so that we can interpolate afterwards
            if np.isnan(missing_animal[0, np.argwhere(np.isnan(centroids[2, :, i_frame]))[m][0]]):
                if backwards:
                    missing_animal[:, np.argwhere(np.isnan(centroids[2, :, i_frame]))[m][0]] = \
                        centroids[0:2, np.argwhere(np.isnan(centroids[2, :, i_frame]))[m][0], i_frame + 1]
                else:
                    missing_animal[:, np.argwhere(np.isnan(centroids[2, :, i_frame]))[m][0]] = \
                        centroids[0:2, np.argwhere(np.isnan(centroids[2, :, i_frame]))[m][0], i_frame - 1]

    #print(centroids[0:2,:,i_frame])
    # for m in range(centroids.shape[1]):
    #    ax.scatter(x=centroids[1,m,i_frame], y=centroids[0,m,i_frame])
    # plt.show()

    plot_image.set_data(smooth_images[:, :, i_frame])
    fig.canvas.restore_region(plot_background)

    for scat in scats:
        scat.remove()
    scats = []

    for m in range(centroids.shape[1]):
        scats.append(ax.scatter(x=centroids[1, m, i_frame], y=centroids[0, m, i_frame], c=cmap(m), s=10-(0.1*m)))
    # ax.draw_artist(plot_background)
    plt.draw()
    plt.pause(0.01)
    #plt.pause(0.25)
    print('lost animals: ' + repr(number_of_frames_animal_lost))

fig = plt.figure()
ax=fig.add_subplot(111)
ax.imshow(background, cmap='Greys_r')
for i in range(centroids.shape[1]):
    ax.scatter(x=centroids[1,i,~np.isnan(centroids[1,i,:])], y=centroids[0,i,~np.isnan(centroids[1,i,:])], c=cmap(i))

plt.draw()

plt.figure()
plt.scatter(x=np.arange(centroids[0,0,~np.isnan(centroids[0,0,:])].shape[-1]), y=centroids[0,0,~np.isnan(centroids[0,0,:])])
plt.grid()
plt.show()

import pandas as pd
s = pd.Series(centroids[0,1,:])

plt.figure()
plt.scatter(x=np.arange(centroids.shape[-1]), y=s.interpolate())
plt.grid()
plt.show()

csv_object = pd.DataFrame({'Frame': np.arange(0, centroids.shape[2])})

for i in range(centroids.shape[1]):
    csv_object['X-Centroid, Animal#' + repr(i)] = centroids[1, i, :]
    csv_object['Y-Centroid, Animal#' + repr(i)] = centroids[0, i, :]

csv_object.astype(int).to_csv('XY_postion.csv', sep=',')

# interpolate data - as I only have data from frame 100 start there (no useful info before as all are NaN
csv_object[100::].interpolate().astype(int).to_csv('XY_position_interpolated.csv', sep=',')