__author__ = 'David Tadres'

import matplotlib.pyplot as plt

'''
Idea:
We want to make the size of the search box optimal. To always capture the animal we have to take the following into 
consideration:
(D. mel larva is the example here)
# Size of the animal in moving direction (L): 5mm # No not necessary, right?
maximal velocity of the animal (v): 1.5mm/s
pixel per mm with the setup (P): i.e. currently 5.5px/mm
fps (F): e.g. 5

The search box should : V/F * P meaning the animal could wander off 1.5mm/s/5fps*5.5px/mm = 0.3mm or 1.65px per frame.
 
'''
def search_box_sim(V,F,P):
    return(V/F*P)

# d. mel larva
V = 1.5
P = 5.5
color = 'blue'
name = 'Drosophila melanogaster larva, v_max: ' + repr(V) + 'mm/s'

fps_result_dmel_larva = []
for i in range(1,80,1):
    fps_result_dmel_larva.append(search_box_sim(V,i,P))

fig = plt.figure(figsize=(7,7))
ax = fig.add_subplot(111)
ax.plot(fps_result_dmel_larva, label = name, color = color)

# D. mel walking
V = 50 # todo: comes from: http://jeb.biologists.org/content/213/14/2494
P = 5.5
color = 'red'
name = 'Drosophila melanogaster walking, v_max: ' + repr(V) + 'mm/s'

fps_result_dmel_walking = []
for i in range(1,80,1):
    fps_result_dmel_walking.append(search_box_sim(V,i,P))

ax.plot(fps_result_dmel_walking, label = name, color = color)

# D. rerio
V = 100 # no idea, i just know they're fast!
P = 5.5
color = 'green'
name = 'Danio rerio 5dpf larva, v_max: ' + repr(V) + 'mm/s'

fps_result_dmel_swimming = []
for i in range(1,80,1):
    fps_result_dmel_swimming.append(search_box_sim(V,i,P))

ax.plot(fps_result_dmel_swimming, label = name, color = color)

# set y-axis limit to a useful limit
ax.set_ylim([0,40])

# current max fps of the tracking algorithm
current_max_fps_tracking_software = 37
ax.axvline(current_max_fps_tracking_software, color = 'k', linestyle = ':')
ax.annotate('current max fps of \nthe tracking software', xy=(current_max_fps_tracking_software, 1), xytext=(current_max_fps_tracking_software + 10, 1.5),
            arrowprops=dict(facecolor='black', shrink=0.05),
            )

ax.set_ylabel('Size of search box [pixel]')
ax.set_xlabel('Frames per second')
ax.set_title('Shows how large the area is that an animal \nwith a given v_max can go at a given framerate')

ax.legend()
plt.grid()
plt.show(block=True)
