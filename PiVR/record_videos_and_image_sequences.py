__author__ = 'David Tadres'
__project__ = 'PiVR'

import datetime
import json
import os
import time
import tkinter as tk
from pathlib import Path

import numpy as np
import pandas as pd

# this try-except statement checks if the processor is a ARM
# processor (used by the Raspberry Pi) or not. Since this command only
# works in Linux it is caught using try-except otherwise it's throw
# an error in a Windows system.
try:
    if os.uname()[4][:3] == 'arm':
        Raspberry = True
    else:
        Raspberry = False
except AttributeError:
    Raspberry = False

YOCTO=True

if Raspberry:
    # try..except
    # add the AddOns to the system path for easy import statment below
    if YOCTO:
        import sys
        # Add Ons
        packages_path = Path(Path(os.path.dirname(os.path.realpath('__file__'))), 'AddOns', 'Yoctopuce')
        sys.path.insert(0, str(packages_path))

        import read_voltage
        import read_generic_sensor

class RecordVideo():
    def __init__(self,
                 genotype='Unknown',
                 recording_framerate=2,
                 display_framerate = None,
                 resolution=[640, 480],
                 recordingtime=None,
                 signal=None,
                 cam = None,
                 base_path=None,
                 time_dependent_stim_file=None,
                 #stim_GPIO=None,
                 pwm_object=None,
                 model_organism=None,
                 pixel_per_mm=None,
                 output_channel_one=[],
                 output_channel_two=[],
                 output_channel_three=[],
                 output_channel_four=[],
                 high_power_led_bool=False,
                 pwm_range=None,
                 background_channel=[],
                 background_2_channel=[],
                 background_dutycycle=[],
                 background_2_dutycycle=[],
                 version_info = None,
                 voltage_bool=False,
                 generic_bool=False,
                 DAC=False,
                 DAC_zero=None,
                 ):
        """
        changed this by (mis?)-using the motion detector class that
        comes with the picamera module.
        It enables me to do stuff after capturing each frame while
        compressing the video to h264 format (seems to be quite
        lossy, but test!) Good thing is that with this script we are
        able to go to almost 90fps while recording a video at 640*480
        and also controlling the LEDs and other, none CPU intensive
        stuff.
       """

        self.genotype = genotype
        self.recording_framerate = recording_framerate
        self.resolution = resolution
        self.recording_time = recordingtime
        self.cam = cam
        self.path = base_path
        self.time_dependent_stim_file = time_dependent_stim_file
        #self.stim_GPIO=stim_GPIO
        self.pwm_object = pwm_object
        self.output_channel_one = output_channel_one
        self.output_channel_two = output_channel_two
        self.output_channel_three = output_channel_three
        self.output_channel_four = output_channel_four
        self.high_power_led_bool = high_power_led_bool
        self.pwm_range = pwm_range
        self.background_channel = background_channel
        self.background_2_channel = background_2_channel
        self.background_dutycycle = background_dutycycle
        self.background_2_dutycycle = background_2_dutycycle
        self.version_info = version_info

        self.yocto_voltage = voltage_bool
        self.yocto_generic = generic_bool

        self.DAC = DAC
        self.DAC_zero = DAC_zero

        print(self.yocto_voltage)

        self.datetime = time.strftime("%Y.%m.%d_%H-%M-%S")

        os.makedirs(self.path + '/' + self.datetime + '_' + self.genotype, exist_ok=True)  # make the directory
        os.chdir(self.path + '/' + self.datetime + '_' + self.genotype)

        experiment_info = {'PiVR info (recording)': self.version_info,
                           'Camera Shutter Speed [us]': self.cam.exposure_speed,
                           'Experiment Date and Time': self.datetime,
                           'Framerate': recording_framerate,
                           'Pixel per mm': pixel_per_mm,
                           'Exp. Group': genotype,
                           'Resolution': resolution,
                           'Recording time': recordingtime,
                           'Model Organism': model_organism,
                           'output channel 1' : self.output_channel_one,
                           'output channel 2' : self.output_channel_two,
                           'output channel 3' : self.output_channel_three,
                           'output channel 4' : self.output_channel_four,
                           'backlight channel' : self.background_channel,
                           'backlight 2 channel' : self.background_2_channel,
                           'backlight dutycycle' : self.background_dutycycle,
                           'backlight 2 dutcycle' : self.background_2_dutycycle,
                           }

        if self.time_dependent_stim_file is not None:
            self.time_dependent_stim_file.to_csv(self.path + '/' + self.datetime + '_' +
                                                 self.genotype + '/stimulation_used.csv')

        with open((self.path + '/' + self.datetime + '_' + self.genotype + '/' + 'experiment_settings.json'), 'w') as file:
            json.dump(experiment_info, file, sort_keys=True, indent=4)

        self.recording_video_w_output()

    def recording_video_w_output(self):

        '''
        After playing around with other option, using the h264
        encoder directly on the GPU is by far the fastest option.
        At 640x480 it can sustain over 85fps for a very long time -
        especially if only a small part of the image is changing over
        time (which would be the case in many scientific tracking
        situations). Using the motion_output argument that comes with
        the picamera functions I was able to sneak in the pwm_object.
        This means that we can not only record at very high speed
        but that we can also turn on the LED to any intensity at the
        same speed as we are recording the video
        :return:
        '''

        print('recording video at ' + repr(self.recording_framerate) + ' fps for at total time of ' +
              repr(self.recording_time))

        # set the framerate (shouldn't be necessary)
        self.cam.framerate = self.recording_framerate
        # start counting time
        start = time.time()
        # start the video recording. The motion output parameter is the easiest way I found to 'do something'
        # after each frame has been taken. Specifically time is recorded and if there's a stimulation, the GPIO's
        # are turned on or off.
        self.cam.start_recording(self.genotype + '_Video.h264', format='h264',
                                 motion_output=ModifyFrameVideo(
                                 pwm_object= self.pwm_object,
                                 recording_time=self.recording_time,
                                 recording_framerate=self.recording_framerate,
                                 time_dependent_stim_file=self.time_dependent_stim_file,
                                 output_channel_one=self.output_channel_one,
                                 output_channel_two=self.output_channel_two,
                                 output_channel_three=self.output_channel_three,
                                 output_channel_four=self.output_channel_four,
                                 datetime=self.datetime,
                                 high_power_led_bool=self.high_power_led_bool,
                                 pwm_range=self.pwm_range,
                                 cam=self.cam,
                                 yocto_voltage=self.yocto_voltage,
                                 yocto_generic=self.yocto_generic,
                                 DAC=self.DAC,
                                 DAC_zero=self.DAC_zero
                                 )
                                 )

        self.cam.wait_recording(self.recording_time)
        self.cam.stop_recording()
        finish = time.time()
        print('Captured '+repr(self.recording_framerate*self.recording_time)+ ' images at %.2ffps' %
              (self.recording_framerate*self.recording_time / (finish - start)))


    '''
    def record_video(self):
        """
        A placeholder function which can be used to in case the other function breaks. This will only record a video
        and should work out of the box. Decreased functionality, i.e. no light control possible.
        :return:
        """
        # make sure the framerate is as expected
        self.cam.framerate = self.recording_framerate
        # start recording
        self.cam.start_recording(repr(self.genotype) + 'Video.h264', format='h264')
        # Next block everything so that the recording can get along. Prefereable to time.sleep because:
        # It is recommended that this method is called while recording to check for exceptions.
        # http://picamera.readthedocs.io/en/release-1.10/api_camera.html
        self.cam.wait_recording(self.recording_time)
        # After the video has been recorded, stop the recording
        self.cam.stop_recording() 
    '''

class ModifyFrameVideo(object):
    """
    This class is called after each frame is taken. It saves the
    current time since the start of the recording.
    If the user defined a stimulation protocol it will also update
    GPIO's voltage accordingly.
    """
    def __init__(self,
                 pwm_object = None,
                 recording_time=None,
                 recording_framerate=None,
                 time_dependent_stim_file=None,
                 #stim_GPIO=None,
                 output_channel_one=[],
                 output_channel_two=[],
                 output_channel_three=[],
                 output_channel_four=[],
                 datetime='Not defined',
                 high_power_led_bool=False,
                 pwm_range=None,
                 cam=None,
                 yocto_voltage = False,
                 yocto_generic = False,
                 DAC = False,
                 DAC_zero = None
                 ):

        self.size = 0
        self.counter = 0
        self.pwm_object = pwm_object
        self.recording_time=recording_time
        self.recording_framerate=recording_framerate
        self.start_time = time.time() # todo clean up this code!
        self.time_dependent_stim_file = time_dependent_stim_file
        #self.stim_GPIO = stim_GPIO
        self.previous_channel_one_value = None
        self.previous_channel_two_value = None
        self.previous_channel_three_value = None
        self.previous_channel_four_value = None
        self.output_channel_one = output_channel_one
        self.output_channel_two = output_channel_two
        self.output_channel_three = output_channel_three
        self.output_channel_four = output_channel_four
        self.datetime = datetime
        self.high_power_led_bool = high_power_led_bool
        self.pwm_range=pwm_range
        self.cam=cam
        self.yocto_voltage = yocto_voltage
        self.yocto_generic = yocto_generic

        self.DAC = DAC
        self.DAC_zero = DAC_zero


        # Pre-allocate empty array to save timepoint of each recorded frame
        self.real_time = np.zeros((int(self.recording_framerate*self.recording_time)))
        self.stimulation = np.zeros((4, int(self.recording_framerate*self.recording_time)))

        if self.yocto_voltage:
            # Pre-allocate empty array for voltage measurement
            self.measured_voltage = np.zeros((int(
                self.recording_framerate*self.recording_time)))
            # Pre-allocate empty array for current measurement
        if self.yocto_generic:
            self.measured_generic_value = np.zeros((int(
                self.recording_framerate*self.recording_time)))

    def convert_DAC(self, target, DAC_zero):
        """
        This is not going to be generally useful but I think it can
        be easily adapted: This function was written specifically for
        an instrument that can provide -100..+100V by getting a 0-5V
        input.
        The user provides a stim file with the target voltage (e.g.
        -60V).
        This function takes the target value and the DAC_zero value
        that has been manually defined and converts it to a 12bit number
        that is then feed to the MCP4275
        :param target:
        :return:
        """
        return(int(round(DAC_zero + (DAC_zero/100)*-target)))

    def update_pwm_dutycycle_time_dependent(self,
                                            previous_channel_value,
                                            output_channel_list,
                                            output_channel_name):
        """
        A convenience function for the timedependent stimulation.
        Takes the list with the gpios for a given channel,
        and, in a for loop, updates gpios according to a given channel.
        In the first iteration of the loop it will just set the PWM
        dutcycle according to whatever dutycycle is specified.
        As this function is called as
        'previous_channel_x_value = update_pwm_dutcycle..' it then
        updates the previous_channel_x_value for the next iteration.
        :param previous_channel_value: As the GPIO dutcycle should
        only be updated when the value changes, this holds the
        previous value
        :param output_channel_list: list of gpio for a given channel,
        e.g. GPIO 17 would be [[17,1250]] (1250 is the frequency,
        not used here)
        :param output_channel_name: the channel as a string,
        e.g. 'Channel 1'
        """

        if self.counter == 0 or \
            previous_channel_value != self.time_dependent_stim_file[output_channel_name][self.counter]:

            for i_stim in range(len(output_channel_list)):
                if self.high_power_led_bool:

                    self.pwm_object.set_PWM_dutycycle(
                        user_gpio=output_channel_list[i_stim][0],
                        dutycycle=self.pwm_range\
                                  - self.time_dependent_stim_file[
                                      output_channel_name][self.counter])
                else:
                    self.pwm_object.set_PWM_dutycycle(
                        user_gpio=output_channel_list[i_stim][0],
                        dutycycle=self.time_dependent_stim_file[
                            output_channel_name][self.counter])
        return(self.time_dependent_stim_file[output_channel_name][self.counter])

    def write(self, s):
        """
        This function is called every time a the Pi camera returns a
        frame. It currently saves the time in the real_time array
        which will finally be saved as 'timestamps.csv'
        If a time dependent array is provided it will also update
        the GPIO's accordingly. Same function as in the tracking is
        called.
        :param s: placeholder
        :return:
        """

        print('Frame #: ' + repr(self.counter))
        if self.cam.timestamp:
            try:
                self.real_time[self.counter] = self.cam.timestamp
            except IndexError:
                pass
        # print(self.cam.timestamp)

        if self.time_dependent_stim_file is not None:

            if self.DAC is not None:

                if self.counter == 0 or self.previous_channel_one_value \
                        !=self.time_dependent_stim_file['Channel 1'][self.counter]:
                    print('value changed')

                    # Set the DAC value
                    self.DAC.raw_value = self.convert_DAC(
                        self.time_dependent_stim_file['Channel 1'][self.counter],
                        self.DAC_zero)
                    #self.DAC.normalized_value  = self.time_dependent_stim_file[
                    #    'Channel 1'][self.counter]
                    # Save the value for later reference
                    self.previous_channel_one_value = self.time_dependent_stim_file['Channel 1'][self.counter]

                # save the stimulation delivered to the DAC object
                self.stimulation[0, self.counter] = self.time_dependent_stim_file['Channel 1'][self.counter]

            else:
                try:
                    self.previous_channel_one_value = \
                        self.update_pwm_dutycycle_time_dependent(
                            previous_channel_value=self.previous_channel_one_value,
                            output_channel_list=self.output_channel_one,
                            output_channel_name='Channel 1')
                except KeyError:
                    pass

                try:
                    self.previous_channel_two_value = \
                        self.update_pwm_dutycycle_time_dependent(
                            previous_channel_value=self.previous_channel_two_value,
                            output_channel_list=self.output_channel_two,
                            output_channel_name='Channel 2')
                except KeyError:
                    pass
                try:
                    self.previous_channel_three_value = \
                        self.update_pwm_dutycycle_time_dependent(
                            previous_channel_value=self.previous_channel_three_value,
                            output_channel_list=self.output_channel_three,
                            output_channel_name='Channel 3')
                except KeyError:
                    pass
                try:
                    self.previous_channel_four_value = \
                        self.update_pwm_dutycycle_time_dependent(
                            previous_channel_value=self.previous_channel_four_value,
                            output_channel_list=self.output_channel_four,
                            output_channel_name='Channel 4')
                except KeyError:
                    pass

                try:
                    if len(self.output_channel_one) > 0:
                        # save the stimulation delivered to the pwm.object
                        self.stimulation[0, self.counter] = self.time_dependent_stim_file['Channel 1'][self.counter]
                    if len(self.output_channel_two) > 0:
                        self.stimulation[1, self.counter] = self.time_dependent_stim_file['Channel 2'][self.counter]
                    if len(self.output_channel_three) > 0:
                        self.stimulation[2, self.counter] = self.time_dependent_stim_file['Channel 3'][self.counter]
                    if len(self.output_channel_four) > 0:
                        self.stimulation[3, self.counter] = self.time_dependent_stim_file['Channel 4'][self.counter]
                except KeyError:
                    # This could happen when the stimulus file is shorter than the time of the recording
                    pass

        if self.yocto_voltage:
            self.measured_voltage[self.counter] = read_voltage.return_voltage()
        if self.yocto_generic:
            self.measured_generic_value[self.counter] = read_generic_sensor.return_generic_value()

        self.counter +=1

    def flush(self):

        if self.DAC is not None:
            self.DAC.raw_value = self.DAC_zero

        csv_object = pd.DataFrame({'Frame number' : np.arange(0,self.real_time.shape[0]),
                                    'Time [s]' : self.real_time-self.real_time[0],
                                   'Channel 1' : self.stimulation[0,:],
                                   'Channel 2' : self.stimulation[1,:],
                                   'Channel 3' : self.stimulation[2,:],
                                   'Channel 4' : self.stimulation[3,:]
                                   })

        if self.yocto_voltage:
            csv_object['Measured Voltage'] = self.measured_voltage

        if self.yocto_generic:
            csv_object['Measured Generic Value'] = self.measured_generic_value

        csv_object.to_csv(self.datetime + '_stimulation.csv',sep=',')
        #self.real_time.to_csv('timestamps.csv', sep=',')


class RecordFullFrameImages():
    """
    This class lets the user record images
    """
    def __init__(self,
                 cam = None,
                 base_path = None,
                 genotype = None,
                 recording_framerate = 2,
                 resolution = [640,480],
                 recording_time = 0,
                 pixel_per_mm = None,
                 model_organism = 'Not in list',
                 time_dependent_stim_file = None,
                 #stim_GPIO=None,
                 pwm_object = None,
                 output_channel_one=[],
                 output_channel_two=[],
                 output_channel_three=[],
                 output_channel_four=[],
                 image_format='jpg',
                 high_power_led_bool = 0,
                 pwm_range = 0,
                 background_channel=[],
                 background_2_channel=[],
                 background_dutycycle=[],
                 background_2_dutycycle=[],
                 version_info = None
                 ):

        '''
        :param cam:
        :param base_path:
        :param genotype:
        :param recording_framerate:
        :param resolution:
        :param recording_time:
        :param pixel_per_mm:
        :param model_organism:
        '''

        self.cam = cam
        self.path = base_path
        self.genotype = genotype
        self.recording_framerate = recording_framerate
        self.resolution = resolution
        self.recording_time = recording_time
        self.pixel_per_mm = pixel_per_mm
        self.model_organism = model_organism
        self.time_dependent_stim_file = time_dependent_stim_file
        #self.stim_GPIO = stim_GPIO
        self.pwm_object = pwm_object
        self.trailing_zeros = None
        self.previous_channel_one_value = None
        self.previous_channel_two_value = None
        self.previous_channel_three_value = None
        self.previous_channel_four_value = None
        self.output_channel_one = output_channel_one
        self.output_channel_two = output_channel_two
        self.output_channel_three = output_channel_three
        self.output_channel_four = output_channel_four
        self.image_format = image_format
        self.real_time = None
        self.start_time = None
        self.stimulation = None
        self.high_power_led_bool = high_power_led_bool
        self.pwm_range = pwm_range
        self.background_channel = background_channel
        self.background_2_channel = background_2_channel
        self.background_dutycycle = background_dutycycle
        self.background_2_dutycycle = background_2_dutycycle
        self.version_info = version_info
        self.counter = 0

        self.record_image_sequence_func()

    def update_pwm_dutycycle_time_dependent(self,
                                            previous_channel_value,
                                            output_channel_list,
                                            output_channel_name):
        """
        A convenience function for the time dependent stimulation.
        Takes the list with the gpios for a given channel, and,
        in a for loop, updates gpios according to a given channel.
        In the first iteration of the loop it will just set the PWM
        dutcycle according to whatever dutycycle is specified.
        As this function is called as
        'previous_channel_x_value = update_pwm_dutcycle..' it then
        updates the previous_channel_x_value for the next iteration.
        :param previous_channel_value: As the GPIO dutycycle should
        only be updated when the value changes, this holds the
        previous value
        :param output_channel_list: list of gpio for a given channel,
        e.g. GPIO 17 would be [[17,1250]] (1250 is the frequency,
        not used here)
        :param output_channel_name: the channel as a string,
        e.g. 'Channel 1'
        """
        if self.counter == 0 or \
            previous_channel_value != self.time_dependent_stim_file[output_channel_name][self.counter]:

            for i_stim in range(len(output_channel_list)):
                if self.high_power_led_bool:
                    self.pwm_object.set_PWM_dutycycle(user_gpio=output_channel_list[i_stim][0],
                                                      dutycycle=self.pwm_range -
                                                                self.time_dependent_stim_file[output_channel_name][
                                                                    self.counter])
                else:
                    self.pwm_object.set_PWM_dutycycle(user_gpio=output_channel_list[i_stim][0],
                                                      dutycycle=self.time_dependent_stim_file[output_channel_name][
                                                          self.counter])
        return(self.time_dependent_stim_file[output_channel_name][self.counter])

    def record_image_sequence_func(self):
        datetime = time.strftime("%Y.%m.%d_%H-%M-%S")


        if Raspberry:
            os.makedirs(self.path + '/' + datetime + '_' + self.genotype, exist_ok=True)  # make the directory
            os.chdir(self.path + '/' + datetime + '_' + self.genotype)

        experiment_info = {'PiVR info (recording)': self.version_info,
                           'Camera Shutter Speed [us]': self.cam.exposure_speed,
                           'Experiment Date and Time' : datetime,
                           'Exp. Group': self.genotype,
                           'Framerate': self.recording_framerate,
                           'Resolution': self.resolution,
                           'Recording time': self.recording_time,
                           'Pixel per mm': self.pixel_per_mm,
                           'Model Organism': self.model_organism,
                           'output channel 1' : self.output_channel_one,
                           'output channel 2' : self.output_channel_two,
                           'output channel 3' : self.output_channel_three,
                           'output channel 4' : self.output_channel_four,
                           'backlight channel' : self.background_channel,
                           'backlight 2 channel' : self.background_2_channel,
                           'backlight dutycycle' : self.background_dutycycle,
                           'backlight 2 dutcycle' : self.background_2_dutycycle,
                           }

        # to make it easier to convert images to videos keep filename
        # length, identical. Eg. if we have a total of 25 images it's
        # ok to do image01..image02...image25. But if the total
        # number of images is 1500 the length changes. Better to then
        # use image0001..image1500
        self.trailing_zeros = len(str(self.recording_time*self.recording_framerate))

        with open((self.path + '/' + datetime + '_' + self.genotype +
                   '/' + 'experiment_settings.json'), 'w') as file:
            json.dump(experiment_info, file, sort_keys=True, indent=4)

        #else:
        self.real_time = np.zeros((int(self.recording_time*self.recording_framerate)))
        self.stimulation = np.zeros((4, self.recording_time*self.recording_framerate))

        self.start_time = time.time()
        self.cam.capture_sequence(self.record_sequence_func(),
                                  use_video_port=True)
        finish = time.time()


        csv_object = pd.DataFrame({'Frame number' : np.arange(0,self.real_time.shape[0]),
                                    'Time [s]' : self.real_time-self.start_time,
                                   'Channel 1' : self.stimulation[0,:],
                                   'Channel 2' : self.stimulation[1,:],
                                   'Channel 3' : self.stimulation[2,:],
                                   'Channel 4' : self.stimulation[3,:]
                                   })

        csv_object.to_csv(datetime + '_stimulation.csv',sep=',')

        print('Captured %d frames at %.2ffps' % (int(self.recording_framerate*self.recording_time),
                                                 int(self.recording_framerate * self.recording_time) /
                                                 (finish - self.start_time)))

    def record_sequence_func(self):
        """
        generator function that saves time a frame was taken, adjusts
        GPIO voltage according to a time dependent stimulus file and
        yield images.
        For easier postprocessing the leading zeros of the image (
        e.g. image0001.jpg) is adjusted so that the length
        of the name does not change for one folder.
        """
        while self.counter < int(self.recording_time * self.recording_framerate):
            #timestamps[frame, 1] = time.time()
            #self.real_time['Time'][frame] = time.time() - self.start_time
            self.real_time[self.counter] = time.time()

            if self.time_dependent_stim_file is not None:
                try:
                    self.previous_channel_one_value = \
                        self.update_pwm_dutycycle_time_dependent(
                            previous_channel_value=self.previous_channel_one_value,
                            output_channel_list=self.output_channel_one,
                            output_channel_name='Channel 1')
                except KeyError:
                    pass

                try:
                    self.previous_channel_two_value = \
                        self.update_pwm_dutycycle_time_dependent(
                            previous_channel_value=self.previous_channel_two_value,
                            output_channel_list=self.output_channel_two,
                            output_channel_name='Channel 2')
                except KeyError:
                    pass

                try:
                    self.previous_channel_three_value = \
                        self.update_pwm_dutycycle_time_dependent(
                            previous_channel_value=self.previous_channel_three_value,
                            output_channel_list=self.output_channel_three,
                            output_channel_name='Channel 3')
                except KeyError:
                    pass

                try:
                    self.previous_channel_four_value = \
                        self.update_pwm_dutycycle_time_dependent(
                            previous_channel_value=self.previous_channel_four_value,
                            output_channel_list=self.output_channel_four,
                         output_channel_name='Channel 4')
                except KeyError:
                    pass

                if len(self.output_channel_one) > 0:
                    self.stimulation[0, self.counter] = self.time_dependent_stim_file['Channel 1'][self.counter]
                if len(self.output_channel_two) > 0:
                    self.stimulation[1, self.counter] = self.time_dependent_stim_file['Channel 2'][self.counter]
                if len(self.output_channel_three) > 0:
                    self.stimulation[2, self.counter] = self.time_dependent_stim_file['Channel 3'][self.counter]
                if len(self.output_channel_four) > 0:
                    self.stimulation[3, self.counter] = self.time_dependent_stim_file['Channel 4'][self.counter]


            yield str('image%0' + repr(self.trailing_zeros) + 'd.' + self.image_format) % self.counter

            self.counter += 1

class Timelapse():
    """
    This class lets the user record full frame images for long times
    for framerates below 2fps (the lower limit for other recording
    modes)
    """

    def __init__(self,
                 cam = None,
                 base_path = None,
                 genotype = None,
                 resolution = [640,480],
                 recordingtime = 0,
                 time_between = 0,
                 pixel_per_mm = None,
                 model_organism = 'Not in list',
                 image_format = 'jpg',
                 pwm_range = 0,
                 background_channel = [],
                 background_2_channel = [],
                 background_dutycycle = [],
                 background_2_dutycycle = [],
                 version_info=None):

        self.genotype = genotype
        self.resolution = resolution
        self.total_recording_time = recordingtime
        self.time_between_captures = time_between
        self.pixel_per_mm = pixel_per_mm
        self.model_organism = model_organism
        self.cam = cam
        self.path = base_path
        self.image_format = image_format
        self.pwm_range = pwm_range
        self.background_channel = background_channel
        self.background_2_channel = background_2_channel
        self.background_dutycycle = background_dutycycle
        self.background_2_dutycycle = background_2_dutycycle
        self.version_info = version_info

        datetime = time.strftime("%Y.%m.%d_%H-%M-%S")

        if Raspberry:
            self.current_path = Path(self.path, datetime + '_' + self.genotype)
            self.current_path.mkdir(parents=True, exist_ok=True)
            #os.makedirs(self.path + '/' + datetime + '_' + self.genotype, exist_ok=True)  # make the directory
            #os.chdir(self.path + '/' + datetime + '_' + self.genotype)

        experiment_info = {'PiVR info (recording)': self.version_info,
                           'Camera Shutter Speed [us]': self.cam.exposure_speed,
                           'Experiment Date and Time' : datetime,
                           'Exp. Group': self.genotype,
                           'Resolution': self.resolution,
                           'Total Recording Time[s]': self.total_recording_time,
                           'Time between Images[s]': self.time_between_captures,
                           'Pixel per mm': self.pixel_per_mm,
                           'Model Organism': self.model_organism,
                           'backlight channel' : self.background_channel,
                           'backlight 2 channel' : self.background_2_channel,
                           'backlight dutycycle' : self.background_dutycycle,
                           'backlight 2 dutcycle' : self.background_2_dutycycle,
                           }
        with open((self.path + '/' + datetime + '_' + self.genotype +
                   '/' + 'experiment_settings.json'), 'w') as file:
            json.dump(experiment_info, file, sort_keys=True, indent=4)

        self.child = tk.Toplevel()
        self.child.grab_set()
        # set a title for the window
        self.child.wm_title('Stop Window')
        self.stop_button = tk.Button(self.child,
                                     text='STOP TIMELAPSE',
                                     font="Helvetica 10 bold",
                                     command=self.stop_capture)
        self.stop_button.grid(row=0,column=0)
        self.stop_button.update()

        self.do_capture = True

        self.i_frame = 0

        self.global_start_time = time.time()

        self.control_time_func()

    def control_time_func(self):
        if self.do_capture:
            # collect start time of the loop
            start_time = time.time()
            # Grab the frame
            self.grab_image_func()
            # keep track of number of recorded frames
            self.i_frame += 1
            # if desired time is reached, stop the aquisition
            if time.time() - self.global_start_time >= \
                    self.total_recording_time:
                self.do_capture = False
                print('Timelapse has finished as expected.')
                # Once the while loop has been broken set the main window
                # active again
                self.child.grab_release()
                # close the tkinter window
                self.child.destroy()
            else:
                # if more images need to be collected, get end of
                # loop time
                end_time = time.time()

                time_lost_while_capturing = end_time - start_time
                self.child.after(int(round((self.time_between_captures -
                                  time_lost_while_capturing) * 1000)),
                              self.control_time_func)
        else:
            # Once the while loop has been broken set the main window
            # active again
            self.child.grab_release()
            # close the tkinter window
            self.child.destroy()

    def grab_image_func(self):
        current_time = datetime.datetime.now()
        image_name = current_time.strftime("%Y_%m_%d-%H_%M_%S")\
                           + '.' + self.image_format
        self.cam.capture(str(Path(self.current_path, image_name)))
        print('Frame #' + repr(self.i_frame) + ' captured.')

    def stop_capture(self):
        self.do_capture = False

        # Unsure if necessary to update the experiment setting
        # file...Info is in the images themselves since they are
        # timestamped.

        # Once the while loop has been broken set the main window
        # active again
        self.child.grab_release()
        # close the tkinter window
        self.child.destroy()


